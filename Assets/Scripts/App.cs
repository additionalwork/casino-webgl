﻿using Casino.Models;
using System;
using UnityEngine;

namespace Casino
{
    public class App : MonoBehaviour
    {
        public static App Instance { get; private set; }
        public GameAbstract Game { get; private set; }
        public IAudioManager AudioManager { get; private set; }

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
                return;
            }

            Instance = this;

            AudioManager = GetComponentInChildren<IAudioManager>();

            Game = GetComponentInChildren<GameAbstract>();

            Game.InitializeGame();
        }
    }
}
