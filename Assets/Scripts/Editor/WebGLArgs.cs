using UnityEditor;

namespace Casino.EditorScripts
{
    public class WebGLArgs : Editor
    {
        [MenuItem("Tools/Set WebGL Args")]
        static void SetWebGLArgs()
        {
            PlayerSettings.WebGL.emscriptenArgs = "-s ALLOW_TABLE_GROWTH";
        }
    }
}
