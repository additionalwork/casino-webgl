﻿using System;
using UnityEngine;

namespace Casino.Models
{
    [Serializable]
    public class AudioPair
    {
        public string name;
        public AudioClip clip;
    }
}
