using UnityEngine;
using UnityEngine.Audio;

public class AudioMixerController : MonoBehaviour
{
    [SerializeField] private AudioMixer audioMixer;

    private void Start()
    {
        SetMasterVolume(PlayerPrefs.GetFloat("masterVolume", 1));
        SetMusicVolume(PlayerPrefs.GetFloat("musicVolume", 1));
        SetSoundsVolume(PlayerPrefs.GetFloat("soundsVolume", 1));
    }

    public static float PercentToDecibel(float percent)
    {
        return 20 * Mathf.Log10(percent);
    }

    public void SetMasterVolume(float percent)
    {
        audioMixer.SetFloat("MasterVolume", PercentToDecibel(percent));
    }

    public void SetMusicVolume(float percent)
    {
        audioMixer.SetFloat("MusicVolume", PercentToDecibel(percent));
    }

    public void SetSoundsVolume(float percent)
    {
        audioMixer.SetFloat("SoundsVolume", PercentToDecibel(percent));
    }
}
