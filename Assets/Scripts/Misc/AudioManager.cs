using Casino.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioManager : MonoBehaviour, IAudioManager
{
    [SerializeField]
    private float smoothDuraion;

    [SerializeField]
    private AudioSource audioSourceSounds;

    [SerializeField]
    private AudioSource audioSourceMusic;

    [SerializeField]
    private List<AudioPair> pairs = new List<AudioPair>();

    private Dictionary<string, AudioClip> clipsDict;
    private Coroutine coroutine;

    void Start()
    {
        clipsDict = pairs.ToDictionary(key => key.name, value => value.clip);
    }

    public void ToggleMute(bool enable)
    {
        audioSourceSounds.mute = enable;
        audioSourceMusic.mute = enable;
    }

    public void PlayMusic(string name)
    {
        if (clipsDict.TryGetValue(name, out AudioClip clip))
        {
            PlayMusic(clip);
        }
    }

    public void PlayMusic(AudioClip clip)
    {
        PlayAudio(audioSourceMusic, clip, smoothDuraion, 1, true);
    }

    public void StopMusic()
    {
        StopAudio(audioSourceMusic, smoothDuraion);
    }

    public void PlaySound(string name, bool looping)
    {
        if (clipsDict.TryGetValue(name, out AudioClip clip))
        {
            PlaySound(clip, looping);
        }
    }

    public void PlaySound(AudioClip clip, bool looping)
    {
        PlayAudio(audioSourceSounds, clip, smoothDuraion, 1, looping);
    }

    public void StopSound()
    {
        StopAudio(audioSourceSounds, smoothDuraion);
    }

    public void PlayAudio(AudioSource audioSource, AudioClip audioClip, float smoothDuration = 0f, float targetVolume = 1f, bool looping = false)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
        }

        coroutine = StartCoroutine(ToPlayVolumeSmooth(audioSource, audioClip, smoothDuration, targetVolume, looping));
    }

    public void StopAudio(AudioSource audioSource, float smoothDuration = 0f)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
        }

        coroutine = StartCoroutine(ToStopVolumeSmooth(audioSource, smoothDuration));
        audioSource.Stop();
        audioSource.volume = 1;
    }

    private IEnumerator ToStopVolumeSmooth(AudioSource audioSource, float duration)
    {
        float startVolume = audioSource.volume;
        var currentTimer = 0f;
        while (currentTimer < duration)
        {
            currentTimer += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(startVolume, 0f, currentTimer / duration);
            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    private IEnumerator ToPlayVolumeSmooth(AudioSource audioSource, AudioClip audioClip, float duration, float targetVolume, bool looping)
    {
        yield return ToStopVolumeSmooth(audioSource, duration);

        audioSource.clip = audioClip;
        audioSource.loop = looping;
        audioSource.Play();

        var currentTimer = 0f;
        while (currentTimer < duration)
        {
            currentTimer += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(0f, targetVolume, currentTimer / duration);
            yield return new WaitForEndOfFrame();
        }
    }
}
