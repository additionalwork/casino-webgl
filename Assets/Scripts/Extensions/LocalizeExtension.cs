﻿using System;
using UnityEngine;
using UnityEngine.Localization.Components;
using UnityEngine.UI;

namespace Casino.Extensions
{
    public static class LocalizeExtension
    {
        public static void SetLocalizedString(this GameObject obj, LocalizeStringEntry textEntry, bool toRefreshSmart = false)
        {
            if (!obj.TryGetComponent<LocalizeStringEvent>(out var localizedString))
            {
#if DEBUG
                Debug.LogError("Object \"" + obj.name + "\" has no \"LocalizeStringEvent\" component!");
#endif
                return;
            }

            var updatedMono = localizedString.OnUpdateString.GetPersistentTarget(0) as MonoBehaviour;
            if (updatedMono == null || !updatedMono.gameObject.Equals(obj))
            {
#if DEBUG
                Debug.LogError("Object " + obj.name + " has wrong \"OnUpdateString\" target!");
#endif
                return;
            }

            if (textEntry.TableName == null)
            {
#if DEBUG
                Debug.LogError("TableName " + "\"" + textEntry.TableName + "\"" + " is null!");
#endif
                return;
            }

            if (localizedString.StringReference.TableEntryReference.Key == textEntry.TextKey &&
                localizedString.StringReference.TableReference.TableCollectionName == textEntry.TableName &&
                !toRefreshSmart)
            {
#if DEBUG
                Debug.LogWarning("Something went wrong during localization!");
#endif
                localizedString.RefreshString();
                return;
            }

            if (textEntry.TableName != null && textEntry.TableName != string.Empty)
            {
                localizedString.StringReference.TableReference = textEntry.TableName;
            }

            localizedString.StringReference.Arguments = new object[] { textEntry.Args };
            localizedString.StringReference.TableEntryReference = textEntry.TextKey;
            localizedString.RefreshString();
        }

        public static void SetLocalizedString(this Text obj, LocalizeStringEntry textEntry, bool toRefreshSmart = false)
        {
            SetLocalizedString(obj.gameObject, textEntry, toRefreshSmart);
        }
    }
}
