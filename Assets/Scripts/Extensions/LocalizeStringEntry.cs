﻿using System;
using System.Collections.Generic;

namespace Casino.Extensions
{
    public class LocalizeStringEntry
    {
        public string TextKey { get; private set; }
        public string TableName { get; private set; } = string.Empty;
        public Dictionary<string, string> Args { get; private set; }

        public LocalizeStringEntry(string key)
        {
            TextKey = key;
        }

        public LocalizeStringEntry(string key, string tableName) : this(key)
        {
            TableName = tableName;
        }

        public LocalizeStringEntry(string key, string tableName, Dictionary<string, string> args) : this(key, tableName)
        {
            Args = args;
        }
    }
}
