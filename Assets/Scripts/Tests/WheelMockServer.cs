using Casino.Models;
using Casino.Models.Wheel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Casino.Tests
{
    public class WheelMockServer : IGameMockServer
    {
        private float pingTime = 0.06f;
        private long CurrentBalance = 123456;
        private List<float> Multipliers;

        public WheelMockServer()
        {
            SetupMockServer();
        }

        public void SetupMockServer()
        {
            Multipliers = new List<float>()
            {
                0f, 0.25f, 0.5f, 0.75f, 1f, 1.25f, 1.5f, 1.75f, 2f,
                0f, 0.25f, 0.5f, 0.75f, 1f, 1.25f, 1.5f, 1.75f, 2f,
            };
        }

        public async void GetGameSettingsWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            SetupMockServer();

            var settings = JsonUtility.FromJson<GetGameSettingsDTO>(data);

            IDataResponse response = new GetGameSettingsDTOResponse
            {
                MinBidStep = 5000,
                Multipliers = Multipliers.ToArray(),
            };

            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void GetBalanceWebGLAsync(Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            callback.Invoke(JsonUtility.ToJson(new GetBalanceDTOResponse { Balance = CurrentBalance }));
        }

        public async void StartGameWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }
            
            IDataResponse response = null;

            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void GetStepResultWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }
            var sectorNumber = UnityEngine.Random.Range(0, Multipliers.Count);
            var response = new GetStepResultDTOResponse
            {
                SectorNumber = sectorNumber,
            };

            var stepData = JsonUtility.FromJson<GetStepResultWheelDTO>(data);
            CurrentBalance += (stepData.BetValue * (long)(Multipliers[sectorNumber] * 100)) / 100 - stepData.BetValue;
            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void EndGameWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            IDataResponse response = null;

            callback.Invoke(JsonUtility.ToJson(response));
        }
    }
}
