using Casino.Models;
using Casino.Models.SlotsClassic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Casino.Tests
{
    public class SlotsAdvanceMockServer : IGameMockServer
    {
        private float pingTime = 0.06f;
        private long CurrentBalance = 123456;
        private int[] Multipliers;

        public SlotsAdvanceMockServer()
        {
            SetupMockServer();
        }

        public void SetupMockServer()
        {
            Multipliers = new int[] { 0, 3, 4, 5, };
        }

        public async void GetGameSettingsWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            var settings = JsonUtility.FromJson<GetGameSettingsDTO>(data);

            IDataResponse response = new GetGameSettingsDTOResponse
            {
                MinBidStep = 5000,
            };

            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void GetBalanceWebGLAsync(Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            callback.Invoke(JsonUtility.ToJson(new GetBalanceDTOResponse { Balance = CurrentBalance }));
        }

        public async void StartGameWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }
            
            IDataResponse response = null;

            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void GetStepResultWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            var multiplierIndex = UnityEngine.Random.Range(0, Multipliers.Length);
            var response = new GetStepResultDTOResponse
            {
                Multiplier = Multipliers[multiplierIndex],
            };

            var stepData = JsonUtility.FromJson<GetStepResultSlotsClassicDTO>(data);
            CurrentBalance += (stepData.BetValue * (long)(Multipliers[multiplierIndex] * 100)) / 100 - stepData.BetValue;
            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void EndGameWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            IDataResponse response = null;

            callback.Invoke(JsonUtility.ToJson(response));
        }
    }
}
