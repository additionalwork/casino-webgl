using Casino.Models;
using Casino.Models.Ticket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Casino.Tests
{
    public class TicketMockServer : IGameMockServer
    {
        private float pingTime = 0.06f;
        private const int FieldSize = 60;
        private long CurrentBid;
        private long CurrentAttemptsCount;
        private long CurrentBalance = 123456;
        private int CurrentStep;
        private float TotalMultiplier;
        private bool IsGameStarted = false;
        private List<CellDTO> TicketStates;

        public TicketMockServer()
        {
            SetupMockServer();
        }

        public void SetupMockServer()
        {
            TicketStates = new List<CellDTO>(FieldSize)
            {
                new CellDTO { CellState = TicketCellState.Multiplier, Multiplier = 0.1f },
                new CellDTO { CellState = TicketCellState.Multiplier, Multiplier = 0.25f },
                new CellDTO { CellState = TicketCellState.Empty },
                new CellDTO { CellState = TicketCellState.Multiplier, Multiplier = 2.5f },
                new CellDTO { CellState = TicketCellState.Empty },
                new CellDTO { CellState = TicketCellState.Multiplier, Multiplier = 15f },
                new CellDTO { CellState = TicketCellState.Multiplier, Multiplier = 0.75f },
                new CellDTO { CellState = TicketCellState.Multiplier, Multiplier = 1f },
                new CellDTO { CellState = TicketCellState.Multiplier, Multiplier = 0f },
                new CellDTO { CellState = TicketCellState.Multiplier, Multiplier = 10f }
            };

            var count = FieldSize - TicketStates.Count;
            for (int k = 0; k < count; k++)
            {
                var randomMultiplier = UnityEngine.Random.Range(0, 20) * 0.25f;
                var cell = new CellDTO { CellState = TicketCellState.Multiplier, Multiplier = randomMultiplier };
                TicketStates.Add(cell);
            }

            ResetCurrentGame();
        }

        public void ResetCurrentGame()
        {
            CurrentBid = 0;
            CurrentStep = 0;
            TotalMultiplier = 1;
            IsGameStarted = false;
        }

        public async void GetGameSettingsWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            SetupMockServer();

            var settings = JsonUtility.FromJson<GetGameSettingsDTO>(data);

            IDataResponse response = new GetGameSettingsDTOResponse
            {
                MinBidStep = 10000,
                IsActiveSession = IsGameStarted,
                History = TicketStates.Take(CurrentStep).ToArray(),
            };

            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void GetBalanceWebGLAsync(Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            callback.Invoke(JsonUtility.ToJson(new GetBalanceDTOResponse { Balance = CurrentBalance }));
        }

        public async void StartGameWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            if (IsGameStarted)
            {
                callback.Invoke(JsonUtility.ToJson(new StartGameDTOResponse
                {
                    IsSuccessful = false,
                    ErrorMessage = "Incorrect start game, because the game has already started!",
                }));
                return;
            }

            ResetCurrentGame();

            IsGameStarted = true;

            var settings = JsonUtility.FromJson<StartGameDTO>(data);
            IDataResponse response = null;
            var fullSettings = JsonUtility.FromJson<StartGameTicketDTO>(data);
            var balanceResult = CurrentBalance - fullSettings.BetValue;

            if (balanceResult >= 0 && fullSettings.BetValue > 0)
            {
                CurrentBid = fullSettings.BetValue;
                CurrentAttemptsCount = fullSettings.AttemptsCount;
                CurrentBalance = balanceResult;
                response = new StartGameDTOResponse
                {
                    BidAmount = CurrentBid,
                    IsSuccessful = true,
                };
            }
            else
            {
                response = new StartGameDTOResponse
                {
                    IsSuccessful = false,
                    ErrorMessage = "Not enough money!",
                };
            }

            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void GetStepResultWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            if (!IsGameStarted)
            {
                return;
            }

            var fullClickResult = JsonUtility.FromJson<GetStepResultTicketDTO>(data);

            var state = TicketStates[CurrentStep];
            var response = new GetStepResultDTOResponse
            {
                Col = fullClickResult.Col,
                Row = fullClickResult.Row,
                Type = state.CellState,
                Value = state.Multiplier,
            };

            if (state.CellState == TicketCellState.Multiplier)
            {
                TotalMultiplier *= state.Multiplier;
            }

            CurrentStep += 1;

            if (CurrentStep >= CurrentAttemptsCount)
            {
                CurrentBalance += (CurrentBid * (long)(TotalMultiplier * 100)) / 100;
                ResetCurrentGame();
            }

            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void EndGameWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            CurrentBalance += (CurrentBid * (long)(TotalMultiplier * 100)) / 100;

            var endGame = JsonUtility.FromJson<EndGameDTO>(data);
            IDataResponse response = new EndGameDTOResponse
            {
                IsSuccessful = true,
            };

            ResetCurrentGame();

            callback.Invoke(JsonUtility.ToJson(response));
        }
    }
}
