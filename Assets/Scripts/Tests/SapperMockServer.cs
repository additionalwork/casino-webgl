using Casino.Models;
using Casino.Models.Sapper;
using System;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Casino.Tests
{
    public class SapperMockServer : IGameMockServer
    {
        private float pingTime = 0.06f;
        private const int FieldSize = 5;
        private long CurrentBid;
        private long CurrentMinesCount;
        private long CurrentBalance = 123456;
        private int CurrentStep;
        private float TotalMultiplier;
        private bool IsGameStarted = false;
        private CellDTO[] SapperStates;

        public SapperMockServer()
        {
            SetupMockServer();
        }

        public void SetupMockServer()
        {
            SapperStates = new CellDTO[] {
                new CellDTO { CellState = SapperCellState.Multiplier, Multiplier = 0.1f },
                new CellDTO { CellState = SapperCellState.Multiplier, Multiplier = 0.25f },
                new CellDTO { CellState = SapperCellState.Empty },
                new CellDTO { CellState = SapperCellState.Empty },
                new CellDTO { CellState = SapperCellState.Multiplier, Multiplier = 0.5f },
                new CellDTO { CellState = SapperCellState.Multiplier, Multiplier = 0.75f },
                new CellDTO { CellState = SapperCellState.Multiplier, Multiplier = 1f },
                new CellDTO { CellState = SapperCellState.Multiplier, Multiplier = 2f },
                new CellDTO { CellState = SapperCellState.Multiplier, Multiplier = 2.5f },
                new CellDTO { CellState = SapperCellState.Empty },
                new CellDTO { CellState = SapperCellState.Multiplier, Multiplier = 3f },
                new CellDTO { CellState = SapperCellState.Multiplier, Multiplier = 5f },
                new CellDTO { CellState = SapperCellState.Multiplier, Multiplier = 7.5f },
                new CellDTO { CellState = SapperCellState.Mine },
            };

            ResetCurrentGame();
        }

        public void ResetCurrentGame()
        {
            CurrentBid = 0;
            CurrentStep = 0;
            TotalMultiplier = 1;
            IsGameStarted = false;
        }

        public async void GetGameSettingsWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            SetupMockServer();

            var settings = JsonUtility.FromJson<GetGameSettingsDTO>(data);

            IDataResponse response = new GetGameSettingsDTOResponse
            {
                MinBidStep = 10000,
                MaxBombs = 20,
                MinBombs = 10,
                IsActiveSession = IsGameStarted,
                History = SapperStates.Take(CurrentStep).ToArray(),
            };

            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void GetBalanceWebGLAsync(Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            callback.Invoke(JsonUtility.ToJson(new GetBalanceDTOResponse { Balance = CurrentBalance }));
        }

        public async void StartGameWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            if (IsGameStarted)
            {
                callback.Invoke(JsonUtility.ToJson(new StartGameDTOResponse
                {
                    IsSuccessful = false,
                    ErrorMessage = "Incorrect start game, because the game has already started!",
                }));
                return;
            }

            ResetCurrentGame();

            IsGameStarted = true;

            var settings = JsonUtility.FromJson<StartGameDTO>(data);
            IDataResponse response = null;
            var fullSettings = JsonUtility.FromJson<StartGameSapperDTO>(data);
            var balanceResult = CurrentBalance - fullSettings.BetValue;

            if (balanceResult >= 0 && fullSettings.BetValue > 0)
            {
                CurrentBid = fullSettings.BetValue;
                CurrentMinesCount = fullSettings.MinesCount;
                CurrentBalance = balanceResult;
                response = new StartGameDTOResponse
                {
                    BidAmount = CurrentBid,
                    IsSuccessful = true,
                };
            }
            else
            {
                response = new StartGameDTOResponse
                {
                    IsSuccessful = false,
                    ErrorMessage = "Not enough money!",
                };
            }

            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void GetStepResultWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            if (!IsGameStarted)
            {
                return;
            }

            var clickResult = JsonUtility.FromJson<GetClickResultDTO>(data);
            IDataResponse response = null;
            var fullClickResult = JsonUtility.FromJson<GetStepResultSapperDTO>(data);

            if (SapperStates.Length <= CurrentStep)
            {
                response = new GetStepResultDTOResponse
                {
                    Col = fullClickResult.Col,
                    Row = fullClickResult.Row,
                    Type = SapperCellState.Mine,
                    Value = 0,
                };
            }
            else
            {
                var state = SapperStates[CurrentStep];
                response = new GetStepResultDTOResponse
                {
                    Col = fullClickResult.Col,
                    Row = fullClickResult.Row,
                    Type = state.CellState,
                    Value = state.Multiplier,
                };

                if (state.CellState == SapperCellState.Multiplier)
                {
                    TotalMultiplier += state.Multiplier;
                }

                if (state.CellState == SapperCellState.Mine)
                {
                    CurrentBid = 0;
                    ResetCurrentGame();
                }
            }

            CurrentStep += 1;

            var safeCells = FieldSize * FieldSize - CurrentMinesCount;
            if (CurrentStep >= safeCells)
            {
                CurrentBalance += (CurrentBid * (long)(TotalMultiplier * 100)) / 100;
                ResetCurrentGame();
            }

            callback.Invoke(JsonUtility.ToJson(response));
        }

        public async void EndGameWebGLAsync(string data, Action<string> callback)
        {
            float startTime = Time.time;
            while (Time.time < startTime + pingTime)
            {
                await Task.Yield();
            }

            CurrentBalance += (CurrentBid * (long)(TotalMultiplier * 100)) / 100;

            var endGame = JsonUtility.FromJson<EndGameDTO>(data);
            IDataResponse response = new EndGameDTOResponse
            {
                IsSuccessful = true,
            };

            ResetCurrentGame();

            callback.Invoke(JsonUtility.ToJson(response));
        }
    }
}
