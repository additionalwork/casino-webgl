﻿using System;

namespace Casino.Tests
{
    public interface IGameMockServer
    {
        public abstract void GetGameSettingsWebGLAsync(string data, Action<string> callback);
        public abstract void GetBalanceWebGLAsync(Action<string> callback);
        public abstract void StartGameWebGLAsync(string data, Action<string> callback);
        public abstract void GetStepResultWebGLAsync(string data, Action<string> callback);
        public abstract void EndGameWebGLAsync(string data, Action<string> callback);
    }
}
