﻿using System;

namespace Casino.Models.Sapper
{
    [Serializable]
    public struct StartGameDTOResponse : IDataResponse
    {
        public long BidAmount;
        public bool IsSuccessful;
        public string ErrorMessage;
    }
}
