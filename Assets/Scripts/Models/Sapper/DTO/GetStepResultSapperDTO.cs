﻿using System;

namespace Casino.Models.Sapper
{
    [Serializable]
    public class GetStepResultSapperDTO : GetClickResultDTO
    {
        public int Row;
        public int Col;
    }
}
