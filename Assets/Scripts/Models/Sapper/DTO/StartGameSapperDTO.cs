﻿namespace Casino.Models.Sapper
{
    public class StartGameSapperDTO : StartGameDTO
    {
        public long BetValue;
        public int MinesCount;
    }
}
