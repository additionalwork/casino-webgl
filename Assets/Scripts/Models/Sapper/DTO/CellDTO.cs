﻿using System;

namespace Casino.Models.Sapper
{
    [Serializable]
    public struct CellDTO
    {
        public int Row;
        public int Col;
        public SapperCellState CellState;
        public float Multiplier;
    }
}
