﻿using System;

namespace Casino.Models.Sapper
{
    [Serializable]
    public struct GetGameSettingsDTOResponse : IDataResponse
    {
        public long MinBidStep;
        public long BitStep;
        public int MinBombs;
        public int MaxBombs;
        public bool IsActiveSession;
        public CellDTO[] History;
    }
}
