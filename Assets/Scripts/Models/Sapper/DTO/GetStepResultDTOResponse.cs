﻿using System;

namespace Casino.Models.Sapper
{
    [Serializable]
    public struct GetStepResultDTOResponse : IDataResponse
    {
        public int Row;
        public int Col;
        public SapperCellState Type;
        public float Value;
    }
}
