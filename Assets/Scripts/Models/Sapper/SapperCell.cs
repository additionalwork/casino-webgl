﻿using System;
using UnityEngine;

namespace Casino.Models.Sapper
{
    public class SapperCell
    {
        public float MultiplierValue = 0f;
        public SapperCellState CellState = SapperCellState.Unknown;
    }
}
