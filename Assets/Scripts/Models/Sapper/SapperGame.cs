﻿using System;
using UnityEngine;

namespace Casino.Models.Sapper
{
    public class SapperGame : GameAbstract
    {
        private int minBombs = 5;
        public int MinBombs => minBombs;

        private int maxBombs = 20;
        public int MaxBombs => maxBombs;

        private long currentBid;
        public long CurrentBid => currentBid;

        private float totalMultiplier = 1f;
        public float TotalMultiplier => totalMultiplier;

        private int currentGameStep = 0;
        public int CurrentGameStep
        {
            get => currentGameStep;
            set => currentGameStep = value;
        }

        public bool IsGameResolved => currentGameStep >= FieldSize * FieldSize - SelectedMinesCount;

        private int selectedMinesCount = 15;
        public int SelectedMinesCount
        {
            get => selectedMinesCount;
            set => selectedMinesCount = Math.Clamp(value, MinBombs, MaxBombs);
        }

        private const int fieldSize = 5;
        public int FieldSize => fieldSize;

        private const string gameID = "Sapper";
        public override string GameID => gameID;

        private SapperCell[,] sapperCells;
        public SapperCell this[int row, int column] => sapperCells[row, column];

        private protected override IDataResponse GetGameSettingsHandle(string responseData)
        {
            var settings = JsonUtility.FromJson<GetGameSettingsDTOResponse>(responseData);
            ResetGame();
            if (settings.IsActiveSession)
            {
                RestoreFromHistory(settings);
            }

            minBidStep = settings.MinBidStep;
            minBombs = settings.MinBombs;
            maxBombs = settings.MaxBombs;
            IsGetSettings = true;
#if DEBUG
            Debug.Log($"Get game settings: {responseData}");
#endif
            return settings;
        }

        private protected override IDataResponse StartGameHandle(string responseData)
        {
            var startGame = JsonUtility.FromJson<StartGameDTOResponse>(responseData);
            if (startGame.IsSuccessful)
            {
                IsGameStarted = true;
            }

            totalMultiplier = 1f;
            currentBid = startGame.BidAmount;
#if DEBUG
            Debug.Log($"Start game handle: {responseData}");
#endif
            return startGame;
        }

        private protected override IDataResponse GetStepResultHandle(string responseData)
        {
            var stepResult = JsonUtility.FromJson<GetStepResultDTOResponse>(responseData);
            var cell = sapperCells[stepResult.Row, stepResult.Col];
            cell.CellState = stepResult.Type;
            cell.MultiplierValue = stepResult.Value;

            if (cell.CellState == SapperCellState.Multiplier)
            {
                totalMultiplier += cell.MultiplierValue;
            }

            if (cell.CellState == SapperCellState.Mine)
            {
                EndGame();
            }

            if (IsGameResolved)
            {
                EndGame();
            }
#if DEBUG
            Debug.Log($"Get step result handle: {responseData}");
#endif
            return stepResult;
        }

        private protected override IDataResponse EndGameHandle(string responseData)
        {
            ResetGame();
            return base.EndGameHandle(responseData);
        }

        private void ResetGame()
        {
            CurrentGameStep = 0;
            totalMultiplier = 1f;
            sapperCells = new SapperCell[fieldSize, fieldSize];
            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    sapperCells[i, j] = new SapperCell();
                }
            }
        }

        private void RestoreFromHistory(GetGameSettingsDTOResponse settings)
        {
            foreach (var cell in settings.History)
            {
                sapperCells[cell.Row, cell.Col].CellState = cell.CellState;
                sapperCells[cell.Row, cell.Col].MultiplierValue = cell.Multiplier;
            }
        }
    }
}
