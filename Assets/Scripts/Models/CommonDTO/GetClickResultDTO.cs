﻿using System;

namespace Casino.Models
{
    [Serializable]
    public class GetClickResultDTO : IDataRequest
    {
        public string GameId;
        public long UnixTimestamp;
    }
}
