﻿using System;

namespace Casino.Models
{
    [Serializable]
    public struct EndGameDTOResponse : IDataResponse
    {
        public bool IsSuccessful;
    }
}
