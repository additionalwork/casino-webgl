﻿using System;

namespace Casino.Models
{
    [Serializable]
    public struct GetBalanceDTOResponse : IDataResponse
    {
        public long Balance;
    }
}
