﻿using System;

namespace Casino.Models
{
    [Serializable]
    public struct EndGameDTO : IDataRequest
    {
        public string GameId;
    }
}
