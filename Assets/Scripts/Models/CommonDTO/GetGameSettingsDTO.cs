﻿using System;

namespace Casino.Models
{
    [Serializable]
    public struct GetGameSettingsDTO : IDataRequest
    {
        public string GameId;
    }
}
