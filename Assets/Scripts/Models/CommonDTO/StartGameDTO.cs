﻿using System;

namespace Casino.Models
{
    [Serializable]
    public class StartGameDTO : IDataRequest
    {
        public string GameId;
    }
}
