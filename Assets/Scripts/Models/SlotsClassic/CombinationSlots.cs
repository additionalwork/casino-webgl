﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Casino.Models.SlotsClassic
{
    public class CombinationSlots : IEnumerable<Vector2Int>
    {
        private IEnumerator<Vector2Int> enumerator;

        public CombinationSlots(SequenceType sequenceType, Vector2Int startSlot)
        {
            enumerator = new CombinationIterator(sequenceType, startSlot);
        }

        public IEnumerator<Vector2Int> GetEnumerator()
        {
            return enumerator;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        private class CombinationIterator : IEnumerator<Vector2Int>
        {
            private SequenceType sequenceType;
            private Vector2Int startSlot;
            private int sign = 1;

            private Vector2Int current;
            public Vector2Int Current => current;
            object IEnumerator.Current => Current;

            public CombinationIterator(SequenceType sequenceType, Vector2Int startSlot)
            {
                this.sequenceType = sequenceType;
                this.startSlot = startSlot;
                this.current = startSlot;
            }

            public void Dispose() { }

            public bool MoveNext()
            {
                if (current.x >= 5 - 1)
                {
                    return false;
                }

                switch (sequenceType)
                {
                    case SequenceType.Ascending:
                        sign = current.y == 0 || (current.y == 2 && current != startSlot) ? sign * -1 : sign;
                        current = new Vector2Int(current.x + 1, current.y - 1 * sign);
                        break;

                    case SequenceType.Descending:
                        sign = (current.y == 0 && current != startSlot) || current.y == 2 ? sign * -1 : sign;
                        current = new Vector2Int(current.x + 1, current.y + 1 * sign);
                        break;

                    default:
                        current = new Vector2Int(current.x + 1, current.y);
                        break;
                }

                return true;
            }

            public void Reset()
            {
                current = startSlot;
            }
        }
    }
}
