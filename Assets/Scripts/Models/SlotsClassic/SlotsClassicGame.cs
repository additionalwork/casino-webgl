﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Casino.Models.SlotsClassic
{
    public class SlotsClassicGame : GameAbstract
    {
        private const string gameID = "SlotsClassic";
        public override string GameID => gameID;

        private int itemsCount = 9;
        public int ItemsCount => itemsCount;

        private int[][] rollsContent = new int[][]
        {
            new int[] { 3, 8, 2, 6, 5, 1, 0, 7, 4, },
            new int[] { 8, 4, 1, 0, 5, 3, 7, 2, 6, },
            new int[] { 7, 4, 0, 2, 1, 5, 6, 3, 8, },
            new int[] { 8, 3, 2, 6, 4, 1, 7, 0, 5, },
            new int[] { 0, 5, 7, 6, 4, 2, 8, 1, 3, },
        };
        public int[][] RollsContent => rollsContent;

        private int[] currentSlotIndex = new int[5];
        public int[] CurrentSlotIndex => currentSlotIndex;

        private int[] winSlots = new int[5];
        public int[] WinSlots => winSlots;

        private protected override IDataResponse GetGameSettingsHandle(string responseData)
        {
            var settings = JsonUtility.FromJson<GetGameSettingsDTOResponse>(responseData);
            minBidStep = settings.MinBidStep;
            IsGetSettings = true;
#if DEBUG
            Debug.Log($"Get game settings: {responseData}");
#endif
            return settings;
        }

        private protected override IDataResponse StartGameHandle(string responseData)
        {
            return null;
        }

        private protected override IDataResponse GetStepResultHandle(string responseData)
        {
            var stepResult = JsonUtility.FromJson<GetStepResultDTOResponse>(responseData);
            ResetState();
            GenerateRandomWinSlots(stepResult.Multiplier);
            GenerateSlots();
#if DEBUG
            Debug.Log($"Get step result handle: {responseData}");
#endif
            return stepResult;
        }

        private protected override IDataResponse EndGameHandle(string responseData)
        {
            return null;
        }

        private void ResetState()
        {
            winSlots = new int[] { -1, -1, -1, -1, -1, };
            currentSlotIndex = new int[] { -1, -1, -1, -1, -1, };
        }

        private void GenerateRandomWinSlots(int slotsCount)
        {
            if (slotsCount <= 0)
            {
                return;
            }

            var seqType = (SequenceType)UnityEngine.Random.Range(0, 3);
            var row = UnityEngine.Random.Range(0, 3);
            var col = UnityEngine.Random.Range(0, 5 - slotsCount + 1);
            var combination = new CombinationSlots(seqType, new Vector2Int(col, row));
            for (int k = col; k < col + slotsCount; k++)
            {
                winSlots[k] = combination.GetEnumerator().Current.y;
                combination.GetEnumerator().MoveNext();
            }
        }

        private void GenerateSlots()
        {
            var itemId = UnityEngine.Random.Range(0, itemsCount);
            for (int k = 0; k < currentSlotIndex.Length; k++)
            {
                if (winSlots[k] != -1)
                {
                    currentSlotIndex[k] = RollsContent[k]
                        .Select((item, index) => new { slot = item, slotIndex = index })
                        .Where(item => item.slot == itemId).First().slotIndex;

                    currentSlotIndex[k] -= WinSlots[k];
                    if (currentSlotIndex[k] < 0)
                    {
                        currentSlotIndex[k] += itemsCount;
                    }
                }
                else
                {
                    currentSlotIndex[k] = UnityEngine.Random.Range(0, itemsCount);

                }
            }

            for (int k = 0; k < currentSlotIndex.Length; k++)
            {
                if (winSlots[k] != -1)
                {
                    continue;
                }

                var collisionCounter = 0;
                while (HasBadNearbies())
                {
#if DEBUG
                    Debug.Log($"Collision resolve on {k} roll!");
#endif
                    collisionCounter++;
                    if (collisionCounter >= 100)
                    {
#if DEBUG
                        Debug.LogError($"Solving item collisions failed after {collisionCounter} attempts on {k} roll!");
#endif
                        break;
                    }
                }
            }
        }

        private bool HasBadNearbies()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    var combination = new List<Vector2Int>();
                    var checkItemId = rollsContent[i][GetValidSlotIndex(currentSlotIndex[i] + j)];
                    var counter = 0;
                    combination.Add(new Vector2Int(i, j));

                    for (int k = i + 1; k < 5; k++)
                    {
                        if (checkItemId == rollsContent[k][GetValidSlotIndex(currentSlotIndex[k])] && combination[counter].y != 2)
                        {
                            counter++;
                            combination.Add(new Vector2Int(k, 0));
                            continue;
                        }

                        if (checkItemId == rollsContent[k][GetValidSlotIndex(currentSlotIndex[k] + 1)])
                        {
                            counter++;
                            combination.Add(new Vector2Int(k, 1));
                            continue;
                        }

                        if (checkItemId == rollsContent[k][GetValidSlotIndex(currentSlotIndex[k] + 2)] && combination[counter].y != 0)
                        {
                            counter++;
                            combination.Add(new Vector2Int(k, 2));
                            continue;
                        }

                        break;
                    }

                    if (counter < 2)
                    {
                        continue;
                    }

                    foreach (var item in combination)
                    {
                        if (winSlots[item.x] == -1)
                        {
                            currentSlotIndex[item.x] = GetValidSlotIndex(currentSlotIndex[item.x] + 2);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool HasBadCombination()
        {
            var wasShuffled = false;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    for (int sequenceType = 0; sequenceType < 3; sequenceType++)
                    {
                        //var wasShuffled = false;
                        var checkItemId = rollsContent[i][j];
                        var combination = new CombinationSlots((SequenceType)sequenceType, new Vector2Int(i, j));
                        var overlaps = new List<Vector2Int>();
                        var counter = 0;

                        foreach (var slot in combination)
                        {
                            if (rollsContent[slot.x][slot.y] == checkItemId)
                            {
                                Debug.Log($"checkItemId: {checkItemId} | slot: {rollsContent[slot.x][slot.y]} | counter: {counter}");
                                overlaps.Add(new Vector2Int(i + counter, j));
                                counter++;
                            }
                        }

                        if (overlaps.Count >= 3)
                        {
                            foreach (var rollIndex in overlaps.Select(vector => vector.x))
                            {
                                if (winSlots[rollIndex] == -1)
                                {
                                    currentSlotIndex[rollIndex] = GetValidSlotIndex(currentSlotIndex[rollIndex] + 1);
                                    wasShuffled = true;
                                }
                            }
                        }
                    }
                }
            }

            return wasShuffled;
        }

        private bool СontinuousСombination(SequenceType sequenceType, Vector2Int checkSlot)
        {
            var wasShuffled = false;
            var checkItemId = rollsContent[checkSlot.x][checkSlot.y];
            var combination = new CombinationSlots(sequenceType, new Vector2Int(checkSlot.x, checkSlot.y));
            var overlaps = new List<Vector2Int>();
            var counter = 0;

            foreach (var slot in combination)
            {
                if (rollsContent[slot.x][slot.y] == checkItemId)
                {
                    overlaps.Add(new Vector2Int(checkSlot.x + counter, checkSlot.y));
                    counter++;
                }
            }

            if (overlaps.Count >= 3)
            {
                foreach (var rollIndex in overlaps.Select(vector => vector.x))
                {
                    if (winSlots[rollIndex] == -1)
                    {
                        currentSlotIndex[rollIndex] = GetValidSlotIndex(currentSlotIndex[rollIndex] + 1);
                        wasShuffled = true;
                    }
                }
            }

            return wasShuffled;
        }

        private int CountNextOverlaps(Vector2Int prev, int itemId, int counter)
        {
            counter++;
            var prevItem = RollsContent[prev.x][GetValidSlotIndex(currentSlotIndex[prev.x])];
            var nextItem = RollsContent[prev.x + 1][GetValidSlotIndex(currentSlotIndex[prev.x] + 1)];

            if (prevItem == nextItem)
            {
                return CountNextOverlaps(prev, itemId, counter);
            }
            else
            {
                return counter;
            }
        }

        private bool HasNearbyDuplicate(int rollIndex)
        {
            // Check left side
            if ((rollIndex - 1 >= 0) && (currentSlotIndex[rollIndex - 1] != -1))
            {
                for (int k = 0; k < 3; k++)
                {
                    var currentItem = RollsContent[rollIndex][GetValidSlotIndex(currentSlotIndex[rollIndex] + k)];
                    for (int n = -1; n <= 1; n++)
                    {
                        if ((k == 0 && n == -1) || (k == 2 && n == 1))
                        {
                            continue;
                        }
                        var nearItem = RollsContent[rollIndex - 1][GetValidSlotIndex(currentSlotIndex[rollIndex - 1] + n)];
                        if (currentItem == nearItem)
                        {
                            return true;
                        }
                    }
                }
            }

            // Check right side
            if ((rollIndex + 1 <= 4) && (currentSlotIndex[rollIndex + 1] != -1))
            {
                for (int k = 0; k < 3; k++)
                {
                    var currentItem = RollsContent[rollIndex][GetValidSlotIndex(currentSlotIndex[rollIndex] + k)];
                    for (int n = -1; n <= 1; n++)
                    {
                        if ((k == 0 && n == -1) || (k == 2 && n == 1))
                        {
                            continue;
                        }
                        var nearItem = RollsContent[rollIndex + 1][GetValidSlotIndex(currentSlotIndex[rollIndex + 1] + n)];
                        if (currentItem == nearItem)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private int GetValidSlotIndex(int slotIndex)
        {
            if (slotIndex >= itemsCount)
            {
                return slotIndex - itemsCount;
            }

            if (slotIndex < 0)
            {
                return slotIndex + itemsCount;
            }

            return slotIndex;
        }
    }
}
