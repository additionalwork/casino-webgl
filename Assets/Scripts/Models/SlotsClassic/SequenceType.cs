﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Casino.Models.SlotsClassic
{
    public enum SequenceType : byte
    {
        Line = 0,
        Ascending = 1,
        Descending = 2,
    }
}
