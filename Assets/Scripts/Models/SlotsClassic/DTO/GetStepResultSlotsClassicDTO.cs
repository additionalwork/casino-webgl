﻿using System;

namespace Casino.Models.SlotsClassic
{
    [Serializable]
    public class GetStepResultSlotsClassicDTO : GetClickResultDTO
    {
        public long BetValue;
    }
}
