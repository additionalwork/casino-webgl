﻿using System;
using System.Collections.Generic;

namespace Casino.Models.SlotsClassic
{
    [Serializable]
    public struct GetGameSettingsDTOResponse : IDataResponse
    {
        public long MinBidStep;
    }
}
