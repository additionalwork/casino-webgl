﻿using System;

namespace Casino.Models.SlotsClassic
{
    [Serializable]
    public struct GetStepResultDTOResponse : IDataResponse
    {
        public int Multiplier;
    }
}
