﻿using System;
using UnityEngine;

namespace Casino.Models.Wheel
{
    public class WheelGame : GameAbstract
    {
        private int currentSectorIndex;
        public int CurrentSectorIndex => currentSectorIndex;

        private const string gameID = "Wheel";
        public override string GameID => gameID;

        private float[] sectors;
        public float this[int index] => sectors[index];

        public int SectorsCount => sectors.Length;

        private protected override IDataResponse GetGameSettingsHandle(string responseData)
        {
            var settings = JsonUtility.FromJson<GetGameSettingsDTOResponse>(responseData);

            minBidStep = settings.MinBidStep;
            sectors = settings.Multipliers;
            IsGetSettings = true;
#if DEBUG
            Debug.Log($"Get game settings: {responseData}");
#endif
            return settings;
        }

        private protected override IDataResponse StartGameHandle(string responseData)
        {
            return null;
        }

        private protected override IDataResponse GetStepResultHandle(string responseData)
        {
            var stepResult = JsonUtility.FromJson<GetStepResultDTOResponse>(responseData);
            currentSectorIndex = stepResult.SectorNumber;
#if DEBUG
            Debug.Log($"Get step result handle: {responseData}");
#endif
            return stepResult;
        }

        private protected override IDataResponse EndGameHandle(string responseData)
        {
            return null;
        }
    }
}
