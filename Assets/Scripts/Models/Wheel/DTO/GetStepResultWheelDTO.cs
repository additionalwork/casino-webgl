﻿using System;

namespace Casino.Models.Wheel
{
    [Serializable]
    public class GetStepResultWheelDTO : GetClickResultDTO
    {
        public long BetValue;
    }
}
