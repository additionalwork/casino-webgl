﻿using System;

namespace Casino.Models.Wheel
{
    [Serializable]
    public struct GetStepResultDTOResponse : IDataResponse
    {
        public int SectorNumber;
    }
}
