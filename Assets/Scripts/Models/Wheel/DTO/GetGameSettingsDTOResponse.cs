﻿using System;

namespace Casino.Models.Wheel
{
    [Serializable]
    public struct GetGameSettingsDTOResponse : IDataResponse
    {
        public long MinBidStep;
        public float[] Multipliers;
    }
}
