﻿using System;
using UnityEngine;

namespace Casino.Models
{
    public interface IAudioManager
    {
        public void PlaySound(string name, bool looping);
        public void PlaySound(AudioClip clip, bool looping);
        public void StopSound();
        public void PlayMusic(string name);
        public void PlayMusic(AudioClip clip);
        public void StopMusic();
        public void ToggleMute(bool enable);
    }
}
