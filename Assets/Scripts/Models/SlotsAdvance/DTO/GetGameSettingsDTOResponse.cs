﻿using System;
using System.Collections.Generic;

namespace Casino.Models.SlotsAdvance
{
    [Serializable]
    public struct GetGameSettingsDTOResponse : IDataResponse
    {
        public long MinBidStep;
    }
}
