﻿using System;

namespace Casino.Models.SlotsAdvance
{
    [Serializable]
    public struct GetStepResultDTOResponse : IDataResponse
    {
        public int Multiplier;
    }
}
