﻿using System;

namespace Casino.Models.SlotsAdvance
{
    [Serializable]
    public class GetStepResultSlotsAdvanceDTO : GetClickResultDTO
    {
        public long BetValue;
    }
}
