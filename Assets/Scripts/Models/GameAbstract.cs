﻿using AOT;
using System;
using UnityEngine;

namespace Casino.Models
{
    public abstract class GameAbstract : MonoBehaviour
    {
        private protected long minBidStep = 10000;
        public long MinBidStep => minBidStep;

        private long selectedBidAmount;
        public long SelectedBidAmount
        {
            get => selectedBidAmount;
            set
            {
                if (Balance < MinBidStep)
                {
                    selectedBidAmount = 0;
                }
                else
                {
                    selectedBidAmount = Math.Clamp(value, MinBidStep, (Balance / MinBidStep) * MinBidStep);
                }
            }
        }

        public abstract string GameID { get; }
        public long Balance { get; private set; }
        public bool IsGetSettings { get; private protected set; } = false;
        public bool IsGameStarted { get; private protected set; } = false;

        public event Action<IDataResponse> OnGameSettingsAction;
        public event Action<IDataResponse> OnBalanceAction;
        public event Action<IDataResponse> OnStartGameAction;
        public event Action<IDataResponse> OnStepResultAction;
        public event Action<IDataResponse> OnEndGameAction;

        public void InitializeGame()
        {
#if DEBUG
            Debug.Log($"Game loaded.");
#endif
            var dataObject = new GetGameSettingsDTO { GameId = GameID, };
            GetGameSettings(dataObject);
            UpdateBalance();
        }

        public void GetGameSettings(IDataRequest data)
        {
            IsGameStarted = false;
            var requestString = JsonUtility.ToJson(data);
            JSCall.GetGameSettings(requestString, GetGameSettingsCallback);
            UpdateBalance();
        }

        private protected abstract IDataResponse GetGameSettingsHandle(string responseData);

        public void UpdateBalance()
        {
            JSCall.GetBalance(GetBalanceCallback);
        }

        private IDataResponse UpdateBalanceHandle(string responseData)
        {
            var balance = JsonUtility.FromJson<GetBalanceDTOResponse>(responseData);
            Balance = balance.Balance;
#if DEBUG
            Debug.Log($"Update balance handle: {responseData}");
#endif
            if (SelectedBidAmount > Balance)
            {
                SelectedBidAmount = (Balance / MinBidStep) * MinBidStep;
            }

            return balance;
        }

        public void StartGame(IDataRequest data)
        {
            if (IsGameStarted)
            {
                return;
            }

            var requestString = JsonUtility.ToJson(data);
            JSCall.StartGame(requestString, StartGameCallback);
            UpdateBalance();
        }

        private protected abstract IDataResponse StartGameHandle(string responseData);

        public void GetStepResult(IDataRequest data)
        {
            var requestString = JsonUtility.ToJson(data);
            JSCall.GetStepResult(requestString, GetStepResultCallback);
            UpdateBalance();
        }

        private protected abstract IDataResponse GetStepResultHandle(string responseData);

        public void EndGame()
        {
            IsGameStarted = false;
            var requestString = JsonUtility.ToJson(new EndGameDTO { GameId = GameID });
            JSCall.EndGame(requestString, EndGameCallback);
            UpdateBalance();
        }

        private protected virtual IDataResponse EndGameHandle(string responseData)
        {
            var isSuccesful = JsonUtility.FromJson<EndGameDTOResponse>(responseData);
            if (isSuccesful.IsSuccessful)
            {
                Debug.LogWarning("End game is succesful!");
            }
            else
            {
                Debug.LogWarning("Something went wrong when the game ended!");
            }
#if DEBUG
            Debug.Log($"End game handle: {responseData}");
#endif
            return isSuccesful;
        }

        [MonoPInvokeCallback(typeof(Action))]
        private protected static void GetGameSettingsCallback(string responseString)
        {
            var data = App.Instance.Game.GetGameSettingsHandle(responseString);
#if DEBUG
            Debug.Log("Game setting callback received.");
#endif
            App.Instance.Game.OnGameSettingsAction?.Invoke(data);
        }

        [MonoPInvokeCallback(typeof(Action))]
        private protected static void GetBalanceCallback(string responseString)
        {
            var data = App.Instance.Game.UpdateBalanceHandle(responseString);
#if DEBUG
            Debug.Log("Balance callback received.");
#endif
            App.Instance.Game.OnBalanceAction?.Invoke(data);
        }

        [MonoPInvokeCallback(typeof(Action))]
        private protected static void StartGameCallback(string responseString)
        {
            var data = App.Instance.Game.StartGameHandle(responseString);
#if DEBUG
            Debug.Log("Start game callback received.");
#endif
            App.Instance.Game.OnStartGameAction?.Invoke(data);
        }

        [MonoPInvokeCallback(typeof(Action))]
        private protected static void GetStepResultCallback(string responseString)
        {
            var data = App.Instance.Game.GetStepResultHandle(responseString);
#if DEBUG
            Debug.Log("Step result callback received.");
#endif
            App.Instance.Game.OnStepResultAction?.Invoke(data);
        }

        [MonoPInvokeCallback(typeof(Action))]
        private protected static void EndGameCallback(string responseString)
        {
            var data = App.Instance.Game.EndGameHandle(responseString);
#if DEBUG
            Debug.Log("End game callback received.");
#endif
            App.Instance.Game.OnEndGameAction?.Invoke(data);
        }
    }
}
