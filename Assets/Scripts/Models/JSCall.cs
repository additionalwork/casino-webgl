﻿using Casino.Tests;
using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Casino.Models
{
    public static class JSCall
    {
#if (MOCK_JSLIB)
        private static IGameMockServer currentGame;

        public static void InstantiateGame(string gameId)
        {
            switch (gameId)
            {
                case "Sapper":
                    currentGame = new SapperMockServer();
                    break;

                case "Ticket":
                    currentGame = new TicketMockServer();
                    break;

                case "Wheel":
                    currentGame = new WheelMockServer();
                    break;

                case "SlotsClassic":
                    currentGame = new SlotsClassicMockServer();
                    break;

                case "SlotsAdvance":
                    currentGame = new SlotsClassicMockServer();
                    break;

                default: throw new ArgumentException("Game not found!");
            }
        }

        public static void GetGameSettings(string data, Action<string> callback)
        {
            InstantiateGame(JsonUtility.FromJson<StartGameDTO>(data).GameId);
            currentGame.GetGameSettingsWebGLAsync(data, callback);
        }

        public static void GetBalance(Action<string> callback) => currentGame.GetBalanceWebGLAsync(callback);

        public static void StartGame(string data, Action<string> callback) => currentGame.StartGameWebGLAsync(data, callback);

        public static void GetStepResult(string data, Action<string> callback) => currentGame.GetStepResultWebGLAsync(data, callback);

        public static void EndGame(string data, Action<string> callback) => currentGame.EndGameWebGLAsync(data, callback);
#else
        private static void GetGameSettings(string data, Action<string> callback) => GetGameSettingsWebGL(data, callback);

        private static void GetBalance(Action<string> callback) => GetBalanceWebGL(callback);

        private static void StartGame(string data, Action<string> callback) => StartGameWebGL(data, callback);

        private static void GetStepResult(string data, Action<string> callback) => GetStepResultWebGL(data, callback);

        private static void EndGame(string data, Action<string> callback) => EndGameWebGL(data, callback);
#endif

        [DllImport("__Internal")]
        private static extern void GetGameSettingsWebGL(string data, Action<string> callback);

        [DllImport("__Internal")]
        private static extern void GetBalanceWebGL(Action<string> callback);

        [DllImport("__Internal")]
        private static extern void StartGameWebGL(string data, Action<string> callback);

        [DllImport("__Internal")]
        private static extern void GetStepResultWebGL(string data, Action<string> callback);

        [DllImport("__Internal")]
        private static extern void EndGameWebGL(string data, Action<string> callback);
    }
}
