﻿using System;
using UnityEngine;

namespace Casino.Models.Ticket
{
    public class TicketCell
    {
        public float MultiplierValue = 0f;
        public TicketCellState CellState = TicketCellState.Unknown;
    }
}
