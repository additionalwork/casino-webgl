﻿namespace Casino.Models.Ticket
{
    public enum TicketCellState
    {
        Unknown = 0,
        Empty = 1,
        Multiplier = 2,
    }
}
