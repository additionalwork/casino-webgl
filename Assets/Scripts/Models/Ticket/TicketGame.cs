﻿using System;
using UnityEngine;

namespace Casino.Models.Ticket
{
    public class TicketGame : GameAbstract
    {
        private long currentBid;
        public long CurrentBid => currentBid;

        private float totalMultiplier = 1f;
        public float TotalMultiplier => totalMultiplier;

        private int currentGameStep = 0;
        public int CurrentGameStep
        {
            get => currentGameStep;
            set => currentGameStep = value;
        }

        public bool IsGameResolved => currentGameStep >= SelectedAttemptsCount;

        private int selectedAttemptsCount = 5;
        public int SelectedAttemptsCount
        {
            get => selectedAttemptsCount;
            set => selectedAttemptsCount = Math.Clamp(value, 1, FieldSize);
        }

        private const int fieldSizeX = 6;
        private const int fieldSizeY = 10;
        public int FieldSizeX => fieldSizeX;
        public int FieldSizeY => fieldSizeY;
        public int FieldSize => fieldSizeX * fieldSizeY;

        private const string gameID = "Ticket";
        public override string GameID => gameID;

        private TicketCell[,] ticketCells;
        public TicketCell this[int row, int column] => ticketCells[row, column];

        private protected override IDataResponse GetGameSettingsHandle(string responseData)
        {
            var settings = JsonUtility.FromJson<GetGameSettingsDTOResponse>(responseData);
            ResetGame();
            if (settings.IsActiveSession)
            {
                RestoreFromHistory(settings);
            }

            minBidStep = settings.MinBidStep;
            IsGetSettings = true;
#if DEBUG
            Debug.Log($"Get game settings: {responseData}");
#endif
            return settings;
        }

        private protected override IDataResponse StartGameHandle(string responseData)
        {
            var startGame = JsonUtility.FromJson<StartGameDTOResponse>(responseData);
            if (startGame.IsSuccessful)
            {
                IsGameStarted = true;
            }

            totalMultiplier = 1f;
            currentBid = startGame.BidAmount;
#if DEBUG
            Debug.Log($"Start game handle: {responseData}");
#endif
            return startGame;
        }

        private protected override IDataResponse GetStepResultHandle(string responseData)
        {
            var stepResult = JsonUtility.FromJson<GetStepResultDTOResponse>(responseData);
            var cell = ticketCells[stepResult.Row, stepResult.Col];
            cell.CellState = stepResult.Type;
            cell.MultiplierValue = stepResult.Value;

            if (cell.CellState == TicketCellState.Multiplier)
            {
                totalMultiplier *= cell.MultiplierValue;
            }

            if (IsGameResolved)
            {
                EndGame();
            }
#if DEBUG
            Debug.Log($"Get step result handle: {responseData}");
#endif
            return stepResult;
        }

        private protected override IDataResponse EndGameHandle(string responseData)
        {
            ResetGame();
            return base.EndGameHandle(responseData);
        }

        private void ResetGame()
        {
            CurrentGameStep = 0;
            totalMultiplier = 1f;
            ticketCells = new TicketCell[fieldSizeX, fieldSizeY];
            for (int i = 0; i < fieldSizeX; i++)
            {
                for (int j = 0; j < fieldSizeY; j++)
                {
                    ticketCells[i, j] = new TicketCell();
                }
            }
        }

        private void RestoreFromHistory(GetGameSettingsDTOResponse settings)
        {
            foreach (var cell in settings.History)
            {
                ticketCells[cell.Row, cell.Col].CellState = cell.CellState;
                ticketCells[cell.Row, cell.Col].MultiplierValue = cell.Multiplier;
            }
        }
    }
}
