﻿using System;

namespace Casino.Models.Ticket
{
    [Serializable]
    public struct GetStepResultDTOResponse : IDataResponse
    {
        public int Row;
        public int Col;
        public TicketCellState Type;
        public float Value;
    }
}
