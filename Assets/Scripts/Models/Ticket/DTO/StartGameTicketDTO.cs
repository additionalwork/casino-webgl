﻿namespace Casino.Models.Ticket
{
    public class StartGameTicketDTO : StartGameDTO
    {
        public long BetValue;
        public int AttemptsCount;
    }
}
