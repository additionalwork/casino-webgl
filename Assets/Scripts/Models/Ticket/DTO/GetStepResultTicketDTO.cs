﻿using System;

namespace Casino.Models.Ticket
{
    [Serializable]
    public class GetStepResultTicketDTO : GetClickResultDTO
    {
        public int Row;
        public int Col;
    }
}
