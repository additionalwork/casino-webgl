﻿using System;

namespace Casino.Models.Ticket
{
    [Serializable]
    public struct CellDTO
    {
        public int Row;
        public int Col;
        public TicketCellState CellState;
        public float Multiplier;
    }
}
