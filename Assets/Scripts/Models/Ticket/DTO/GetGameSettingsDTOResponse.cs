﻿using System;

namespace Casino.Models.Ticket
{
    [Serializable]
    public struct GetGameSettingsDTOResponse : IDataResponse
    {
        public long MinBidStep;
        public bool IsActiveSession;
        public CellDTO[] History;
    }
}
