﻿using Casino.Extensions;
using Casino.Models;
using Casino.Models.Sapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Casino.UI
{
    public class SapperMainUI : BaseUI
    {
        private const string localizeTable = "Sapper";
        private readonly Dictionary<float, GameObject> multipliers = new();
        private SapperGame game;
        private GameObject[,] cells;
        private bool isResetAvailable;
        private Queue<float> addedMultipliersToBar = new();

        [SerializeField] private Transform fieldContent;
        [SerializeField] private GameObject unknownCellPrefab;
        [SerializeField] private GameObject boomCellPrefab;
        [SerializeField] private GameObject bombCellPrefab;
        [SerializeField] private GameObject defaultMultiplierCellPrefab;
        [SerializeField] private GameObject[] multiplierPrefabs;

        [Header("BottomBar1")]
        [SerializeField] private Button minesMinus;
        [SerializeField] private Button minesPlus;
        [SerializeField] private TextMeshProUGUI minesCount;
        [SerializeField] private TextMeshProUGUI currentProfit;
        [SerializeField] private TextMeshProUGUI currentProfitInfo;
        [SerializeField] private GameObject darkBar;
        [SerializeField] private GameObject slideBar;
        [SerializeField] private Transform slideBarContent;

        [Header("BottomBar2")]
        [SerializeField] private Button bidMinus;
        [SerializeField] private Button bidPlus;
        [SerializeField] private Button playButton;
        [SerializeField] private GameObject playText;
        [SerializeField] private TextMeshProUGUI bidAmount;
        [SerializeField] private TextMeshProUGUI bidAmountText;

        private protected override void SetupContext()
        {
            game = (SapperGame)baseGame;
            cells = new GameObject[game.FieldSize, game.FieldSize];
            InitMultiplierPrefabs();
            SetupFieldContent();
            PrepareGameToStart();

            minesMinus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedMinesCount--;
                minesCount.text = game.SelectedMinesCount.ToString();
                currentProfit.text = ConvertCurrency(CalcTotalGain()).ToString("F0");
            });
            minesPlus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedMinesCount++;
                minesCount.text = game.SelectedMinesCount.ToString();
                currentProfit.text = ConvertCurrency(CalcTotalGain()).ToString("F0");
            });
            bidMinus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedBidAmount -= game.MinBidStep;
                bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
                currentProfit.text = ConvertCurrency(CalcTotalGain()).ToString("F0");
                SetInteractablePlayButton();
            });
            bidPlus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedBidAmount += game.MinBidStep;
                bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
                currentProfit.text = ConvertCurrency(CalcTotalGain()).ToString("F0");
                SetInteractablePlayButton();
            });
        }

        private long CalcTotalGain()
        {
            return (long)Math.Pow(game.SelectedBidAmount * game.SelectedMinesCount * 1.5d, 1.2d);
        }

        private void SetInteractablePlayButton()
        {
            if (game.SelectedBidAmount < game.MinBidStep)
            {
                playButton.interactable = false;
            }
            else
            {
                playButton.interactable = true;
            }
        }

        private void PrepareGameToStart()
        {
            SwitchPlayButtonState();

            game.SelectedBidAmount = game.MinBidStep;
            minesCount.text = game.SelectedMinesCount.ToString();
            currentProfit.text = ConvertCurrency(CalcTotalGain()).ToString("F0");
            bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
            bidAmount.color = new Color(1f, 1f, 1f, 0.5f);
            bidAmountText.gameObject.SetActive(true);
            currentProfitInfo.gameObject.SetLocalizedString(new LocalizeStringEntry("Max_win", localizeTable));
            addedMultipliersToBar.Clear();
            AddMultiplierToBar(game.TotalMultiplier);

            game.UpdateBalance();
            ToggleLockBars(true);
            SetupFieldContent();
            SetInteractablePlayButton();
        }

        private void SwitchPlayButtonState()
        {
            playButton.interactable = true;

            if (isResetAvailable)
            {
                playText.SetLocalizedString(new LocalizeStringEntry("Restart", localizeTable));
                playButton.onClick.RemoveAllListeners();
                playButton.onClick.AddListener(() =>
                {
                    App.Instance.AudioManager.PlaySound("Return", false);
                    playButton.interactable = false;
                    game.UpdateBalance();
                    isResetAvailable = false;
                    PrepareGameToStart();
                });
            }
            else if (!game.IsGameStarted)
            {
                playText.SetLocalizedString(new LocalizeStringEntry("Play", localizeTable));
                playButton.onClick.RemoveAllListeners();
                playButton.onClick.AddListener(() =>
                {
                    App.Instance.AudioManager.PlaySound("Spin", false);
                    playButton.interactable = false;
                    ToggleLockBars(false);
                    game.UpdateBalance();
                    game.StartGame(new StartGameSapperDTO
                    {
                        GameId = game.GameID,
                        BetValue = game.SelectedBidAmount,
                        MinesCount = game.SelectedMinesCount,
                    });
                });
            }
            else if (game.IsGameStarted)
            {
                playText.SetLocalizedString(new LocalizeStringEntry("Take", localizeTable));
                playButton.onClick.RemoveAllListeners();
                playButton.onClick.AddListener(() =>
                {
                    App.Instance.AudioManager.PlaySound("Spin", false);
                    playButton.interactable = false;
                    ToggleCells(false);
                    game.EndGame();
                });
            }
        }

        private void AddMultiplierToBar(float newMultiplier)
        {
            addedMultipliersToBar.Enqueue(newMultiplier);
            if (addedMultipliersToBar.Count > 4)
            {
                addedMultipliersToBar.Dequeue();
            }

            foreach (Transform transform in slideBarContent)
            {
                transform.gameObject.SetActive(false);
            }

            for (int k = addedMultipliersToBar.Count - 1; k >= 0; k--)
            {
                var multiplierBar = slideBarContent.GetChild(addedMultipliersToBar.Count - 1 - k).gameObject;
                multiplierBar.SetActive(true);
                multiplierBar.GetComponentInChildren<TextMeshProUGUI>().text = "x" + addedMultipliersToBar.ElementAt(k);
            }
        }

        private protected override void GameStartedHandle(IDataResponse dto)
        {
            game.UpdateBalance();
            SwitchPlayButtonState();
            ToggleCells(true);
        }

        private protected override void GetStepCellHandle(IDataResponse dto)
        {
            currentProfitInfo.gameObject.SetLocalizedString(new LocalizeStringEntry("Current_win", localizeTable));
            var stepResult = (GetStepResultDTOResponse)dto;
            GameObject parentObject;
            switch (stepResult.Type)
            {
                case SapperCellState.Empty:
                    parentObject = cells[stepResult.Row, stepResult.Col];
                    parentObject.GetComponentInChildren<Button>().gameObject.SetActive(false);
                    break;

                case SapperCellState.Multiplier:
                    parentObject = cells[stepResult.Row, stepResult.Col];
                    parentObject.GetComponentInChildren<Button>().gameObject.SetActive(false);
                    InstantiateMultiplier(parentObject.transform, (float)Math.Round(stepResult.Value, 2));
                    currentProfit.text = (ConvertCurrency(game.CurrentBid) * game.TotalMultiplier).ToString("F2");
                    AddMultiplierToBar(game.TotalMultiplier);
                    break;

                case SapperCellState.Mine:
                    if (isResetAvailable)
                    {
                        break;
                    }
            
                    isResetAvailable = true;
                    ToggleCells(false);
                    SwitchPlayButtonState();
                    bidAmount.color = new Color(1f, 0f, 0f, 1f);
                    bidAmount.gameObject.SetLocalizedString(new LocalizeStringEntry("You_lose", localizeTable));
                    bidAmountText.gameObject.SetActive(false);

                    parentObject = cells[stepResult.Row, stepResult.Col];
                    parentObject.GetComponentInChildren<Button>().gameObject.SetActive(false);
                    Instantiate(boomCellPrefab, parentObject.transform);
                    break;
            }
            game.CurrentGameStep++;
            if (game.IsGameResolved)
            {
                isResetAvailable = true;
                ToggleCells(false);
                SwitchPlayButtonState();
                game.EndGame();
                bidAmount.color = new Color(0f, 1f, 0f, 1f);
                bidAmount.gameObject.SetLocalizedString(new LocalizeStringEntry("You_win", localizeTable));
                bidAmountText.gameObject.SetActive(false);

                for (int i = 0; i < game.FieldSize; i++)
                {
                    for (int j = 0; j < game.FieldSize; j++)
                    {
                        if (game[i, j].CellState == SapperCellState.Unknown)
                        {
                            var parent = cells[i, j];
                            parent.GetComponentInChildren<Button>().gameObject.SetActive(false);
                            Instantiate(bombCellPrefab, parent.transform);
                        }
                    }
                }
            }
        }

        private protected override void EndGameHandle(IDataResponse dto)
        {
            if (!isResetAvailable)
            {
                PrepareGameToStart();
            }
        }

        private void ToggleCells(bool enable)
        {
            for (int i = 0; i < game.FieldSize; i++)
            {
                for (int j = 0; j < game.FieldSize; j++)
                {
                    if (game[i, j].CellState == SapperCellState.Unknown)
                    {
                        cells[i, j].GetComponentInChildren<Button>().interactable = enable;
                    }
                }
            }
        }

        private void ToggleLockBars(bool enable)
        {
            darkBar.SetActive(enable);
            slideBar.SetActive(!enable);
            bidMinus.gameObject.SetActive(enable);
            bidPlus.gameObject.SetActive(enable);
        }

        private void InstantiateMultiplier(Transform parent, float value)
        {
            if (multipliers.ContainsKey(value))
            {
                Instantiate(multipliers[value], parent);
                return;
            }
            else
            {
                var obj = Instantiate(defaultMultiplierCellPrefab, parent);
                obj.GetComponentInChildren<TextMeshProUGUI>().text = value.ToString("F", CultureInfo.InvariantCulture);
            }
        }

        private void SetupFieldContent()
        {
            for (int i = 0; i < game.FieldSize; i++)
            {
                for (int j = 0; j < game.FieldSize; j++)
                {
                    if (cells[i, j] != null)
                    {
                        Destroy(cells[i, j].gameObject);
                    }

                    var row = i;
                    var col = j;
                    cells[i, j] = Instantiate(unknownCellPrefab, fieldContent);
                    var button = cells[i, j].GetComponentInChildren<Button>();
                    button.onClick.AddListener(() =>
                    {
                        App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                        game.GetStepResult(new GetStepResultSapperDTO
                        {
                            GameId = game.GameID,
                            Row = row,
                            Col = col,
                            UnixTimestamp = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds(),
                        });
                        button.onClick.RemoveAllListeners();
                    });
                }
            }
        }

        private void InitMultiplierPrefabs()
        {
            foreach (var multiplier in multiplierPrefabs)
            {
                if (float.TryParse(multiplier.name, out var value))
                {
                    multipliers.Add(value, multiplier);
                }
            }
        }
    }
}
