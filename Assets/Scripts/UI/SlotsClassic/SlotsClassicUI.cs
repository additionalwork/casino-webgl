﻿using Casino.Extensions;
using Casino.Models;
using Casino.Models.SlotsClassic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Casino.UI
{
    public class SlotsClassicUI : BaseUI
    {
        private const string localizeTable = "Slots";
        private SlotsClassicGame game;
        private const float baseRollSpeed = 200f;
        private double newBalance;
        private float oneItemHeight;
        private List<GameObject> SpawnedBorders = new();

        [SerializeField] private Roll[] rolls;
        [SerializeField] private GameObject[] itemPrefabs;
        [SerializeField] private GameObject borderPrefab;

        [Header("BottomBar1")]
        [SerializeField] private TextMeshProUGUI currentProfit;
        [SerializeField] private TextMeshProUGUI currentProfitInfo;

        [Header("BottomBar2")]
        [SerializeField] private TextMeshProUGUI bidAmount;
        [SerializeField] private Button bidMinus;
        [SerializeField] private Button bidPlus;
        [SerializeField] private Button playButton;

        private protected override void SetupContext()
        {
            game = (SlotsClassicGame)baseGame;
            SetupRollsContent();
            PrepareGameToStart();
            bidMinus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedBidAmount -= game.MinBidStep;
                bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
                SetInteractablePlayButton();
            });
            bidPlus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedBidAmount += game.MinBidStep;
                bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
                SetInteractablePlayButton();
            });
            //playButton.onClick.AddListener(() =>
            //{
            //    App.Instance.AudioManager.PlaySound("Spin", false);
            //    playButton.interactable = false;
            //    ToggleLockBars(false);
            //    ClearBorders();
            //    StopAllCoroutines();
            //    StartCoroutine(ToRotateRolls(10f));
            //    game.GetStepResult(new GetStepResultSlotsClassicDTO
            //    {
            //        GameId = game.GameID,
            //        UnixTimestamp = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds(),
            //        BetValue = game.SelectedBidAmount,
            //    });
            //});
        }

        private void SetInteractablePlayButton()
        {
            if (game.SelectedBidAmount < game.MinBidStep)
            {
                playButton.interactable = false;
            }
            else
            {
                playButton.interactable = true;
            }
        }

        private void PrepareGameToStart()
        {
            game.SelectedBidAmount = game.MinBidStep;
            bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
            ToggleLockBars(true);
            SetInteractablePlayButton();
            SwitchPlayButtonState();
        }

        private void SwitchPlayButtonState()
        {
            playButton.interactable = true;

            if (IsAllRollsStopped())
            {
                var text = playButton.GetComponentInChildren<TextMeshProUGUI>().gameObject;
                text.SetLocalizedString(new LocalizeStringEntry("Spin", localizeTable));
                playButton.onClick.RemoveAllListeners();
                playButton.onClick.AddListener(() =>
                {
                    App.Instance.AudioManager.PlaySound("Spin", false);
                    playButton.interactable = false;
                    ToggleLockBars(false);
                    ClearBorders();
                    StopAllCoroutines();
                    StartCoroutine(ToRotateRolls(10f));
                    game.GetStepResult(new GetStepResultSlotsClassicDTO
                    {
                        GameId = game.GameID,
                        UnixTimestamp = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds(),
                        BetValue = game.SelectedBidAmount,
                    });
                    //isNeedStop = false;
                    //App.Instance.AudioManager.PlaySound("Spin", false);
                    //playButton.interactable = false;
                    //ToggleLockBars(false);
                    //StopAllCoroutines();
                    //StartCoroutine(ToRotateWheel(1f));
                    //game.GetStepResult(new GetStepResultWheelDTO
                    //{
                    //    GameId = game.GameID,
                    //    UnixTimestamp = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds(),
                    //    BetValue = game.SelectedBidAmount,
                    //});
                });
            }
            else
            {
                var text = playButton.GetComponentInChildren<TextMeshProUGUI>().gameObject;
                text.SetLocalizedString(new LocalizeStringEntry("Stop2", localizeTable));
                playButton.onClick.RemoveAllListeners();
                playButton.onClick.AddListener(() =>
                {
                    App.Instance.AudioManager.PlaySound("Spin", false);
                    playButton.interactable = false;
                    foreach (var roll in rolls)
                    {
                        roll.IsNeedStop = true;
                        roll.ToggleWaitBar(true);
                    }
                });
            }
        }

        private bool IsAllRollsStopped()
        {
            foreach (var roll in rolls)
            {
                if (roll.IsRotates)
                {
                    return false;
                }
            }

            return true;
        }

        private protected override void BalanceChangeHandle(IDataResponse dto)
        {
            newBalance = ConvertCurrency(baseGame.Balance);

            if (IsAllRollsStopped())
            {
                balance.text = newBalance.ToString("F");
            }
        }

        private protected override void GetStepCellHandle(IDataResponse dto)
        {
            for (int k = 0; k < rolls.Length; k++)
            {
                rolls[k].ToggleWaitBar(false);
                rolls[k].CurrentItemIndex = game.CurrentSlotIndex[k];
            }
        }

        private void ToggleLockBars(bool enable)
        {
            bidMinus.gameObject.SetActive(enable);
            bidPlus.gameObject.SetActive(enable);
        }

        private void SetupRollsContent()
        {
            if (itemPrefabs.Length == 0)
            {
#if DEBUG
                Debug.LogError("No prefabs for slots!");
#endif
                return;
            }
            
            oneItemHeight = itemPrefabs[0].GetComponent<RectTransform>().sizeDelta.y + 1f;

            for (byte k = 0; k < rolls.Length; k++)
            {
                foreach (Transform sector in rolls[k].RollContent)
                {
                    Destroy(sector.gameObject);
                }

                rolls[k].MainClass = this;
                rolls[k].Items = new();
                var position = UnityEngine.Random.Range(0, itemPrefabs.Length) * oneItemHeight;
                rolls[k].RollContent.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, position);

                var index = k;
                rolls[k].StopButton.GetComponent<Button>().onClick.AddListener(() =>
                {
                    rolls[index].IsNeedStop = true;
                    rolls[index].ToggleWaitBar(true);
                });

                for (int n = 0; n < game.RollsContent[k].Length; n++)
                {
                    rolls[k].Items.Add(Instantiate(itemPrefabs[game.RollsContent[k][n]], rolls[k].RollContent));
                }

                for (int n = 0; n < game.RollsContent[k].Length; n++)
                {
                    rolls[k].Items.Add(Instantiate(itemPrefabs[game.RollsContent[k][n]], rolls[k].RollContent));
                }
            }
        }

        private IEnumerator ToRotateRolls(float targetVelocity)
        {
            balance.text = (newBalance - ConvertCurrency(game.SelectedBidAmount)).ToString("F");
            for (int k = 0; k < rolls.Length; k++)
            {
                rolls[k].ResetState();
                rolls[k].Routine = StartCoroutine(rolls[k].ToRotateRoll(targetVelocity));
            }

            yield return new WaitForSeconds(1f);

            SwitchPlayButtonState();

            var isRotate = true;
            while (isRotate)
            {
                isRotate = rolls.Any(roll => roll.IsRotates);
                yield return new WaitForEndOfFrame();
            }
            
            balance.text = newBalance.ToString("F");
            ToggleLockBars(true);
            bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
            SetInteractablePlayButton();
            SwitchPlayButtonState();
            SpawnBorders();
        }

        private void SpawnBorders()
        {
            for (int k = 0; k < rolls.Length; k++)
            {
                if (game.WinSlots[k] == -1)
                {
                    continue;
                }

                var targetSlot = game.CurrentSlotIndex[k] + game.WinSlots[k];
                if (targetSlot >= game.ItemsCount)
                {
                    targetSlot -= game.ItemsCount;
                }
                var border = Instantiate(borderPrefab, rolls[k].Items[targetSlot].transform);
                SpawnedBorders.Add(border);
                border = Instantiate(borderPrefab, rolls[k].Items[targetSlot + game.ItemsCount].transform);
                SpawnedBorders.Add(border);
            }
        }

        private void ClearBorders()
        {
            foreach (var border in SpawnedBorders)
            {
                Destroy(border);
            }
        }

        [Serializable]
        private class Roll
        {
            public Transform RollContent;
            public GameObject StopButton;
            public GameObject WaitBar;
            [HideInInspector] public List<GameObject> Items;
            [HideInInspector] public bool IsRotates;
            [HideInInspector] public bool IsNeedStop;
            [HideInInspector] public float Velocity;
            [HideInInspector] public Coroutine Routine;
            [HideInInspector] public int CurrentItemIndex;
            [HideInInspector] public SlotsClassicUI MainClass;

            private RectTransform rect;
            private float lastDistance;
            
            public void ResetState()
            {
                ToggleWaitBar(true);
                lastDistance = float.MaxValue;
                IsRotates = false;
                IsNeedStop = false;
                Velocity = 0f;
            }

            public IEnumerator ToRotateRoll(float targetVelocity)
            {
                var startTime = 1f;
                var stopTime = 3f;
                rect = RollContent.GetComponent<RectTransform>();
                IsRotates = true;
                yield return ToStartRotation(targetVelocity, startTime);
                while (!IsNeedStop || !IsAdjustedRotation(stopTime))
                {
                    ToRotateRoll();
                    yield return new WaitForEndOfFrame();
                }
                yield return ToStopRotation(targetVelocity, stopTime);
                IsRotates = false;
            }

            private IEnumerator ToStartRotation(float targetVelocity, float period)
            {
                yield return new WaitForSeconds(UnityEngine.Random.value);
                var timer = 0f;
                while (timer < period)
                {
                    timer += Time.deltaTime;
                    Velocity = targetVelocity * (timer / period);
                    ToRotateRoll();
                    yield return new WaitForEndOfFrame();
                }
            }

            private IEnumerator ToStopRotation(float targetVelocity, float period)
            {
                lastDistance = float.MaxValue;
                var timer = period * (Velocity / targetVelocity);
                var lostBrakingDistance = baseRollSpeed * period * (Velocity / 2f);
                var targetPosition = CurrentItemIndex * MainClass.oneItemHeight;
                
                while (timer > 0f)
                {
                    timer -= Time.deltaTime;
                    Velocity = targetVelocity * (timer / period);
                    lostBrakingDistance -= ToRotateRoll();
                    var isLastCircle = lostBrakingDistance < MainClass.itemPrefabs.Length * MainClass.oneItemHeight;
                    var currentDistance = RollDistance(rect.anchoredPosition.y, targetPosition);
                    var isSlotReached = lastDistance < currentDistance;
                    if (isLastCircle && isSlotReached)
                    {
                        rect.anchoredPosition = new Vector2(0f, targetPosition);
                        break;
                    }
                    else
                    {
                        lastDistance = currentDistance;
                        yield return new WaitForEndOfFrame();
                    }
                }
            }

            private float ToRotateRoll()
            {
                var yMove = Velocity * Time.deltaTime * baseRollSpeed;
                rect.anchoredPosition += new Vector2(0f, yMove);

                var totalRollHeigth = MainClass.itemPrefabs.Length * MainClass.oneItemHeight;
                if (rect.anchoredPosition.y >= totalRollHeigth)
                {
                    rect.anchoredPosition -= new Vector2(0f, totalRollHeigth);
                }

                return yMove;
            }

            private bool IsAdjustedRotation(float period)
            {
                var brakingDist = ResultPath(baseRollSpeed * period * (Velocity / 2f));
                var targetPosition = CurrentItemIndex * MainClass.oneItemHeight - brakingDist;
                if (targetPosition < 0f)
                {
                    targetPosition = MainClass.itemPrefabs.Length * MainClass.oneItemHeight + targetPosition;
                }
                var distance = RollDistance(rect.anchoredPosition.y, targetPosition);
                var isTargetReached = lastDistance < distance;
                lastDistance = distance;
                return isTargetReached;
            }

            private float ResultPath(float distance)
            {
                while (distance > MainClass.itemPrefabs.Length * MainClass.oneItemHeight)
                {
                    distance -= MainClass.itemPrefabs.Length * MainClass.oneItemHeight;
                }

                return distance;
            }

            private float RollDistance(float position, float destination)
            {
                var fullPath = MainClass.itemPrefabs.Length * MainClass.oneItemHeight;

                if (position < destination)
                {
                    return destination - position;
                }
                else
                {
                    return fullPath - position + destination;
                }
            }

            public void ToggleWaitBar(bool enable)
            {
                WaitBar.SetActive(enable);
            }
        }
    }
}
