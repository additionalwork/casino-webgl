﻿using Casino.Models;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Casino.UI
{
    public abstract class BaseUI : MonoBehaviour
    {
        [SerializeField] private Image FrontDecor;
        [SerializeField] private GameObject welcomePanelPrefab;
        [SerializeField] private protected TextMeshProUGUI balance;
        [SerializeField] private Button HowToPlay;

        private protected GameAbstract baseGame;

        private void Start()
        {
            FrontDecor.gameObject.SetActive(true);

            baseGame = App.Instance.Game;

            HowToPlay.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("Return", false);
                var screenCanvas = gameObject.GetComponentInParent<Canvas>().transform;
                Instantiate(welcomePanelPrefab, screenCanvas);
            });

            baseGame.OnEndGameAction += EndGameHandle;
            baseGame.OnBalanceAction += BalanceChangeHandle;
            baseGame.OnStartGameAction += GameStartedHandle;
            baseGame.OnStepResultAction += GetStepCellHandle;
            baseGame.OnGameSettingsAction += GetGameSettingsHandle;
        }

        private void OnDestroy()
        {
            baseGame.OnEndGameAction -= EndGameHandle;
            baseGame.OnBalanceAction -= BalanceChangeHandle;
            baseGame.OnStartGameAction -= GameStartedHandle;
            baseGame.OnStepResultAction -= GetStepCellHandle;
            baseGame.OnGameSettingsAction -= GetGameSettingsHandle;
        }

        private IEnumerator ToShowField()
        {
            while (FrontDecor.color.a > 0)
            {
                var color = FrontDecor.color;
                color.a -= Time.deltaTime * 3f;
                FrontDecor.color = color;
                yield return new WaitForEndOfFrame();
            }

            Destroy(FrontDecor.gameObject);
            FrontDecor.gameObject.SetActive(false);
        }

        private protected double ConvertCurrency(long currency)
        {
            return (double)currency / 100d;
        }

        private protected abstract void SetupContext();

        private protected virtual void GetGameSettingsHandle(IDataResponse dto)
        {
            if (!baseGame.IsGameStarted)
            {
                var screenCanvas = gameObject.GetComponentInParent<Canvas>().transform;
                Instantiate(welcomePanelPrefab, screenCanvas);
            }

            if (FrontDecor != null && FrontDecor.gameObject.activeSelf)
            {
                StartCoroutine(ToShowField());
            }

            SetupContext();
        }

        private protected virtual void BalanceChangeHandle(IDataResponse dto)
        {
            balance.text = ConvertCurrency(baseGame.Balance).ToString("F");
        }

        private protected virtual void GameStartedHandle(IDataResponse dto)
        {
#if DEBUG
            Debug.LogWarning("Behavior undefined - GameStartedHandle!");
#endif
        }

        private protected virtual void GetStepCellHandle(IDataResponse dto)
        {
#if DEBUG
            Debug.LogWarning("Behavior undefined - GameStartedHandle!");
#endif
        }

        private protected virtual void EndGameHandle(IDataResponse dto)
        {
#if DEBUG
            Debug.LogWarning("Behavior undefined - GameStartedHandle!");
#endif
        }
    }
}
