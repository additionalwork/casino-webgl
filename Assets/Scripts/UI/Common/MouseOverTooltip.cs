﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Casino.UI
{
    public class MouseOverTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private GameObject toolTip;

        public void OnPointerEnter(PointerEventData eventData)
        {
            ToggleTooltip(true);
            toolTip.transform.SetParent(GetComponentInParent<BaseUI>().transform);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            ToggleTooltip(false);
            toolTip.transform.SetParent(transform);
        }

        public bool ToggleTooltip(bool enable)
        {
            toolTip.SetActive(enable);
            return enable;
        }

        public bool ToggleTooltip()
        {
            return ToggleTooltip(!toolTip.activeSelf);
        }

        private void Update()
        {
            if (toolTip.activeSelf &&
                EventSystem.current.currentSelectedGameObject != null &&
                !EventSystem.current.currentSelectedGameObject.Equals(gameObject))
            {
                EventSystem.current.SetSelectedGameObject(null);
            }
        }
    }
}
