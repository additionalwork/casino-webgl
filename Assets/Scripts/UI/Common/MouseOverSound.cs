﻿using Casino.Models;
using Casino.Models.Sapper;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Casino.UI
{
    public class MouseOverSound : MonoBehaviour, IPointerEnterHandler
    {
        [SerializeField] private AudioClip clip;

        private IAudioManager audioManager;

        private void Start()
        {
            audioManager = App.Instance.AudioManager;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (clip == null)
            {
                return;
            }

            if (!gameObject.activeSelf)
            {
                return;
            }

            audioManager.PlaySound(clip, false);
        }
    }
}
