﻿using UnityEngine;
using UnityEngine.UI;

namespace Casino.UI
{
    public class WelcomeCloseButton : MonoBehaviour
    {
        [SerializeField] public Button CloseButton;

        private void Awake()
        {
            CloseButton.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                GetComponentInParent<WelcomePanel>().ClosePanel();
            });
        }
    }
}
