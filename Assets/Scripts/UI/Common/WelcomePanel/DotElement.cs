﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Casino.UI
{
    public class DotElement : MonoBehaviour
    {
        [SerializeField] public GameObject ActiveElement;
        [SerializeField] public GameObject InactiveElement;

        public void ActivateElement(bool activate = true)
        {
            ActiveElement.SetActive(activate);
            InactiveElement.SetActive(!activate);
        }
    }
}
