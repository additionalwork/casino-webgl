﻿using Casino.Models.Sapper;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Casino.UI
{
    public class WelcomePanel : MonoBehaviour
    {
        [SerializeField] private GameObject leftArrowPrefab;
        [SerializeField] private GameObject rightArrowPrefab;
        [SerializeField] private GameObject dotElementPrefab;
        [SerializeField] private Transform pagginatorContent;
        [SerializeField] private List<GameObject> panels;
        [SerializeField] private float switchFadeTime = 0.5f;

        private List<DotElement> dots = new();
        private int currentDotIndex = -1;

        private void Start()
        {
            if (panels.Count == 0)
            {
                ClosePanel();
                return;
            }

            SetupContext();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ClosePanel();
            }
        }

        private void SetupContext()
        {
            SetActive(true, panels.ToArray());

            foreach (Transform transform in pagginatorContent)
            {
                Destroy(transform.gameObject);
            }

            var leftArrow = Instantiate(leftArrowPrefab, pagginatorContent);

            var index = 0;
            foreach (var panel in panels)
            {
                var innerIndex = index;
                var dot = Instantiate(dotElementPrefab, pagginatorContent);
                dot.GetComponentInChildren<Button>().onClick.AddListener(() =>
                {
                    App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                    SwitchSlide(innerIndex);
                });
                dots.Add(dot.GetComponent<DotElement>());
                index++;
            }

            var rightArrow = Instantiate(rightArrowPrefab, pagginatorContent);

            leftArrow.GetComponent<Button>().onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                SwitchSlide(forward: false);
            });
            rightArrow.GetComponent<Button>().onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                SwitchSlide(forward: true);
            });

            foreach (var panel in panels)
            {
                if (panel.GetComponentInChildren<Image>().color.a == 0f)
                {
                    continue;
                }

                SetFade(panel, 0f);
            }

            currentDotIndex = panels.Count - 1;

            SwitchSlide();
        }

        private void SwitchSlide(bool forward = true)
        {
            dots[currentDotIndex].ActivateElement(false);

            if (forward)
            {
                currentDotIndex = currentDotIndex + 1 > panels.Count - 1 ? 0 : currentDotIndex + 1;
            }
            else
            {
                currentDotIndex = currentDotIndex - 1 < 0 ? panels.Count - 1 : currentDotIndex - 1;
            }

            dots[currentDotIndex].ActivateElement(true);
            
            StopAllCoroutines();
            StartCoroutine(ToSwitchSlide(panels[currentDotIndex], true));
        }

        private void SwitchSlide(int slideIndex)
        {
            if (slideIndex < 0 || slideIndex > panels.Count - 1)
            {
                return;
            }

            dots[currentDotIndex].ActivateElement(false);
            currentDotIndex = slideIndex;
            dots[currentDotIndex].ActivateElement(true);

            StopAllCoroutines();
            StartCoroutine(ToSwitchSlide(panels[currentDotIndex], true));
        }

        private IEnumerator ToSwitchSlide(GameObject slide, bool instant = false)
        {
            var fade = 1f;
            var opaque = 0f;

            SetActive(true, slide);

            while (opaque < 1f || instant)
            {
                if (instant)
                {
                    fade = 0f;
                    opaque = 1f;
                    instant = false;
                }
                else
                {
                    fade -= Time.deltaTime / switchFadeTime;
                    opaque += Time.deltaTime / switchFadeTime;
                }

                foreach (var panel in panels)
                {
                    if (slide.Equals(panel))
                    {
                        continue;
                    }

                    if (panel.GetComponentInChildren<Image>().color.a == 0f)
                    {
                        continue;
                    }

                    SetFade(panel, fade);
                }

                SetFade(slide, opaque);

                yield return new WaitForEndOfFrame();
            }

            SetActive(false, panels.ToArray());
            SetActive(true, slide);
        }

        private void SetFade(GameObject gameObject, float value)
        {
            var panelImages = gameObject.GetComponentsInChildren<Image>();
            var panelTexts = gameObject.GetComponentsInChildren<TextMeshProUGUI>();
            
            foreach (var image in panelImages)
            {
                image.color = OffsetColorAlpha(image.color, Mathf.Clamp(value, 0f, 1f));
            }

            foreach (var text in panelTexts)
            {
                text.color = OffsetColorAlpha(text.color, Mathf.Clamp(value, 0f, 1f));
            }
        }

        private void SetActive(bool enabled, params GameObject[] gameObjects)
        {
            foreach (var gameObject in gameObjects)
            {
                gameObject.SetActive(enabled);
            }
        }

        private Color OffsetColorAlpha(Color color, float targetAlpha)
        {
            color.a = targetAlpha;
            return color;
        }

        public void ClosePanel()
        {
            Destroy(this.gameObject);
        }
    }
}
