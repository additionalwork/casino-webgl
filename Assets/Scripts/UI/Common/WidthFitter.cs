﻿using UnityEngine;
using UnityEngine.UI;

namespace Casino.UI
{
    public class WidthFitter : MonoBehaviour
    {
        private RectTransform canvasRect;
        private float lastScreenWidth = -1;

        private void Start()
        {
            canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
        }

        private void Update()
        {
            if (canvasRect.rect.width == lastScreenWidth)
            {
                return;
            }

            lastScreenWidth = canvasRect.rect.width;
            var rectTransform = GetComponent<RectTransform>();
            rectTransform.sizeDelta = new Vector2(canvasRect.rect.width, rectTransform.sizeDelta.y);
            var rectTransformLayout = GetComponentInParent<VerticalLayoutGroup>().GetComponent<RectTransform>();
            LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransformLayout);
        }
    }
}
