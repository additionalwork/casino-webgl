﻿using UnityEngine;
using UnityEngine.UI;

namespace Casino.UI
{
    public class RotateByResolution : MonoBehaviour
    {
        private RectTransform canvasRect;
        private Rect lastScreenRect;
        private bool IsDefaultRotation = true;

        private void Start()
        {
            canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
        }

        private void Update()
        {
            if (canvasRect.rect == lastScreenRect)
            {
                return;
            }

            lastScreenRect = canvasRect.rect;

            if (canvasRect.rect.width > canvasRect.rect.height && !IsDefaultRotation)
            {
                IsDefaultRotation = true;
                var rectTransform = GetComponent<RectTransform>();
                rectTransform.rotation = Quaternion.Euler(0f, 0f, 0f);
                rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.y, rectTransform.sizeDelta.x);
            }

            if (canvasRect.rect.width < canvasRect.rect.height && IsDefaultRotation)
            {
                IsDefaultRotation = false;
                var rectTransform = GetComponent<RectTransform>();
                rectTransform.rotation = Quaternion.Euler(0, 0, -90);
                rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.y, rectTransform.sizeDelta.x);
            }
        }
    }
}
