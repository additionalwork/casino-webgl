﻿using UnityEngine;
using UnityEngine.UI;

namespace Casino.UI
{
    public class MuteButton : MonoBehaviour
    {
        [SerializeField] private GameObject EnabledImage;
        [SerializeField] private GameObject DisabledImage;

        private Button button;
        private bool IsMuted = false;

        private void Start()
        {
            App.Instance.AudioManager.ToggleMute(IsMuted);
            EnabledImage.gameObject.SetActive(!IsMuted);
            DisabledImage.gameObject.SetActive(IsMuted);

            button = GetComponent<Button>();
            button.onClick.AddListener(() =>
            {
                IsMuted = !IsMuted;
                App.Instance.AudioManager.ToggleMute(IsMuted);
                EnabledImage.gameObject.SetActive(!IsMuted);
                DisabledImage.gameObject.SetActive(IsMuted);
            });
        }

        private void OnDestroy()
        {
            button.onClick.RemoveAllListeners();
        }
    }
}
