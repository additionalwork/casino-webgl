﻿using Casino.Extensions;
using Casino.Models;
using Casino.Models.Wheel;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Casino.UI
{
    public class WheelUI : BaseUI
    {
        private const string localizeTable = "Wheel";
        private const float baseWheelSpeed = 150f;
        private const float clackAnglePeriod = 10f;
        private float clackAngleCounter = 0f;
        private WheelGame game;
        private GameObject[] sectors;
        private bool isWheelRotates;
        private bool isNeedStop;
        private float wheelVelocity;
        private double newBalance;
        private float currentAngle = 0f;
        
        [SerializeField] private Transform wheel;
        [SerializeField] private GameObject sectorPrefab;
        [SerializeField] private Transform wheelContent;
        [SerializeField] private Animator flipper;

        [Header("RightBar1")]
        [SerializeField] private Button bidMinus;
        [SerializeField] private Button bidPlus;
        [SerializeField] private Button playButton;
        [SerializeField] private GameObject waitBar;
        [SerializeField] private TextMeshProUGUI bidAmount;

        [Header("RightBar2")]
        [SerializeField] private TextMeshProUGUI currentProfit;
        [SerializeField] private TextMeshProUGUI currentProfitInfo;

        private protected override void SetupContext()
        {
            game = (WheelGame)baseGame;
            sectors = new GameObject[game.SectorsCount];
            SetupWheelContent();
            PrepareGameToStart();

            bidMinus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedBidAmount -= game.MinBidStep;
                bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
                SetInteractablePlayButton();
            });
            bidPlus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedBidAmount += game.MinBidStep;
                bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
                SetInteractablePlayButton();
            });
        }

        private void SetInteractablePlayButton()
        {
            if (game.SelectedBidAmount < game.MinBidStep)
            {
                playButton.interactable = false;
            }
            else
            {
                playButton.interactable = true;
            }
        }

        private void PrepareGameToStart()
        {
            SwitchPlayButtonState();

            game.SelectedBidAmount = game.MinBidStep;
            bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");

            ToggleLockBars(true);
            SetInteractablePlayButton();
        }

        private void SwitchPlayButtonState()
        {
            playButton.interactable = true;

            if (!isWheelRotates)
            {
                var text = playButton.GetComponentInChildren<TextMeshProUGUI>().gameObject;
                text.SetLocalizedString(new LocalizeStringEntry("Go", localizeTable));
                playButton.onClick.RemoveAllListeners();
                playButton.onClick.AddListener(() =>
                {
                    isNeedStop = false;
                    App.Instance.AudioManager.PlaySound("Spin", false);
                    playButton.interactable = false;
                    ToggleLockBars(false);
                    StopAllCoroutines();
                    StartCoroutine(ToRotateWheel(1f));
                    game.GetStepResult(new GetStepResultWheelDTO
                    {
                        GameId = game.GameID,
                        UnixTimestamp = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds(),
                        BetValue = game.SelectedBidAmount,
                    });
                });
            }
            else
            {
                var text = playButton.GetComponentInChildren<TextMeshProUGUI>().gameObject;
                text.SetLocalizedString(new LocalizeStringEntry("Stop", localizeTable));
                playButton.onClick.RemoveAllListeners();
                playButton.onClick.AddListener(() =>
                {
                    isNeedStop = true;
                    App.Instance.AudioManager.PlaySound("Spin", false);
                    playButton.interactable = false;
                });
            }
        }

        private void ToggleWaitBar(bool enable)
        {
            playButton.gameObject.SetActive(!enable);
            waitBar.SetActive(enable);
        }

        private protected override void BalanceChangeHandle(IDataResponse dto)
        {
            newBalance = ConvertCurrency(baseGame.Balance);

            if (!isWheelRotates)
            {
                balance.text = newBalance.ToString("F");
            }
        }

        private protected override void GetStepCellHandle(IDataResponse dto)
        {
            SwitchPlayButtonState();
        }

        private void ToggleLockBars(bool enable)
        {
            bidMinus.gameObject.SetActive(enable);
            bidPlus.gameObject.SetActive(enable);
        }

        private void ToggleWinnings(bool enable)
        {
            currentProfitInfo.gameObject.SetActive(enable);
            currentProfit.gameObject.SetActive(enable);
            currentProfit.text = (ConvertCurrency(game.SelectedBidAmount) * game[game.CurrentSectorIndex]).ToString("F");
        }

        private void SetupWheelContent()
        {
            var sectorAngle = 360f / sectors.Length;

            foreach (Transform sector in wheelContent)
            {
                Destroy(sector.gameObject);
            }

            for (int k = 0; k < sectors.Length; k++)
            {
                sectors[k] = Instantiate(sectorPrefab, wheelContent);
                sectors[k].transform.rotation = Quaternion.Euler(0f, 0f, 360f - sectorAngle * k + 90f);
                var text = sectors[k].GetComponentInChildren<TextMeshProUGUI>();
                text.text = $"×{game[k].ToString("F2")}";
                if (k % 2 == 0)
                {
                    text.color = Color.black;
                }
                else
                {
                    text.color = Color.white;
                }
            }
        }

        private IEnumerator ToRotateWheel(float targetVelocity)
        {
            var startTime = 1f;
            var stopTime = 3f;
            isWheelRotates = true;

            yield return ToStartRotation(targetVelocity, startTime);

            var offsetFactor = UnityEngine.Random.value;

            while (!isNeedStop || !IsAdjustedRotation(stopTime, offsetFactor))
            {
                wheel.rotation = GetWheelRotation();
                yield return new WaitForEndOfFrame();
            }
            
            yield return ToStopRotation(targetVelocity, stopTime);

            isWheelRotates = false;

            SwitchPlayButtonState();
            bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
            SetInteractablePlayButton();
        }

        private IEnumerator ToStartRotation(float targetVelocity, float period)
        {
            balance.text = (newBalance - ConvertCurrency(game.SelectedBidAmount)).ToString("F");
            ToggleWinnings(false);
            var timer = 0f;
            while (timer < period)
            {
                timer += Time.deltaTime;
                wheelVelocity = targetVelocity * (timer / period);
                wheel.rotation = GetWheelRotation();
                yield return new WaitForEndOfFrame();
            }
        }

        private IEnumerator ToStopRotation(float targetVelocity, float period)
        {
            var timer = period * (wheelVelocity / targetVelocity);
            while (timer > 0f)
            {
                timer -= Time.deltaTime;
                wheelVelocity = targetVelocity * (timer / period);
                wheel.rotation = GetWheelRotation();
                yield return new WaitForEndOfFrame();
            }
            balance.text = newBalance.ToString("F");
            ToggleWinnings(true);
            ToggleLockBars(true);
        }

        private bool IsAdjustedRotation(float period, float offsetFactor)
        {
            var deadZone = 0.05f;
            var sectorRadius = 360f / game.SectorsCount;
            var semiSectorRadiusDeadZone = sectorRadius / 2f * (1f - deadZone);
            var offset = sectorRadius * (1f - deadZone) * offsetFactor;
            var targetAngle = ResultAngle(game.CurrentSectorIndex * sectorRadius) - offset;
            var brakingDist = baseWheelSpeed * period * (wheelVelocity / 2f);
            var angleCulc = ResultAngle(currentAngle - brakingDist);
            var distance = RotationAngleDist(angleCulc, targetAngle);
            return distance <= semiSectorRadiusDeadZone && targetAngle - angleCulc <= 0f;
        }

        private float ResultAngle(float angle)
        {
            while (angle > 180f)
            {
                angle -= 360f;
            }

            while (angle < -180f)
            {
                angle += 360f;
            }

            return angle;
        }

        private float RotationAngleDist(float angle, float targetAngle)
        {
            var difference = angle - targetAngle;
            return angle > targetAngle ? difference : difference + 360;
        }

        private Quaternion GetWheelRotation()
        {
            var angle = baseWheelSpeed * Time.deltaTime * wheelVelocity;
            currentAngle = ResultAngle(currentAngle - angle);
            if (clackAngleCounter >= clackAnglePeriod)
            {
                clackAngleCounter = 0f;
                App.Instance.AudioManager.PlaySound("Clack", false);
                flipper.SetTrigger("Flip");
            }
            clackAngleCounter += angle;
            return Quaternion.Euler(0f, 0f, currentAngle);
        }
    }
}
