﻿using Casino.Extensions;
using Casino.Models;
using Casino.Models.Ticket;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Casino.UI
{
    public class TicketUI : BaseUI
    {
        private const string localizeTable = "Ticket";
        private readonly List<GameObject> cellBackgrounds = new();
        private TicketGame game;
        private GameObject[,] cells;
        private bool isResetAvailable;
        private int clickedCells = 0;

        [SerializeField] private Transform fieldContent;
        [SerializeField] private GameObject unknownCellPrefab;
        [SerializeField] private GameObject[] cellBackgroundPrefabs;

        [Header("BottomBar1")]
        [SerializeField] private Button attemptsMinus;
        [SerializeField] private Button attemptsPlus;
        [SerializeField] private TextMeshProUGUI attemptsCount;
        [SerializeField] private TextMeshProUGUI currentProfit;
        [SerializeField] private TextMeshProUGUI currentProfitInfo;
        [SerializeField] private GameObject darkBar;

        [Header("BottomBar2")]
        [SerializeField] private Button bidMinus;
        [SerializeField] private Button bidPlus;
        [SerializeField] private Button playButton;
        [SerializeField] private GameObject playText;
        [SerializeField] private TextMeshProUGUI bidAmount;
        [SerializeField] private TextMeshProUGUI bidAmountText;

        private protected override void SetupContext()
        {
            game = (TicketGame)baseGame;
            cells = new GameObject[game.FieldSizeX, game.FieldSizeY];
            InitCellBackgrounds();
            SetupFieldContent();
            PrepareGameToStart();

            attemptsMinus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedAttemptsCount--;
                attemptsCount.text = game.SelectedAttemptsCount.ToString();
                currentProfit.text = ConvertCurrency(CalcTotalGain()).ToString("F0");
            });
            attemptsPlus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedAttemptsCount++;
                attemptsCount.text = game.SelectedAttemptsCount.ToString();
                currentProfit.text = ConvertCurrency(CalcTotalGain()).ToString("F0");
            });
            bidMinus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedBidAmount -= game.MinBidStep;
                bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
                currentProfit.text = ConvertCurrency(CalcTotalGain()).ToString("F0");
                SetInteractablePlayButton();
            });
            bidPlus.onClick.AddListener(() =>
            {
                App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                game.SelectedBidAmount += game.MinBidStep;
                bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
                currentProfit.text = ConvertCurrency(CalcTotalGain()).ToString("F0");
                SetInteractablePlayButton();
            });
        }

        private long CalcTotalGain()
        {
            return (long)Math.Pow(game.SelectedBidAmount * game.SelectedAttemptsCount * 1.5d, 1.2d);
        }

        private void SetInteractablePlayButton()
        {
            if (game.SelectedBidAmount < game.MinBidStep)
            {
                playButton.interactable = false;
            }
            else
            {
                playButton.interactable = true;
            }
        }

        private void PrepareGameToStart()
        {
            SwitchPlayButtonState();

            clickedCells = 0;
            game.SelectedBidAmount = game.MinBidStep;
            attemptsCount.text = game.SelectedAttemptsCount.ToString();
            currentProfit.text = ConvertCurrency(CalcTotalGain()).ToString("F0");
            bidAmount.text = ConvertCurrency(game.SelectedBidAmount).ToString("F2");
            bidAmount.color = new Color(1f, 1f, 1f, 0.5f);
            bidAmountText.gameObject.SetActive(true);
            currentProfitInfo.gameObject.SetLocalizedString(new LocalizeStringEntry("Max_win", localizeTable));

            game.UpdateBalance();
            ToggleLockBars(true);
            SetupFieldContent();
            SetInteractablePlayButton();
        }

        private void SwitchPlayButtonState()
        {
            playButton.interactable = true;

            if (isResetAvailable)
            {
                playText.SetLocalizedString(new LocalizeStringEntry("Restart", localizeTable));
                playButton.onClick.RemoveAllListeners();
                playButton.onClick.AddListener(() =>
                {
                    App.Instance.AudioManager.PlaySound("Return", false);
                    playButton.interactable = false;
                    game.UpdateBalance();
                    isResetAvailable = false;
                    PrepareGameToStart();
                });
            }
            else if (!game.IsGameStarted)
            {
                playText.SetLocalizedString(new LocalizeStringEntry("Play", localizeTable));
                playButton.onClick.RemoveAllListeners();
                playButton.onClick.AddListener(() =>
                {
                    App.Instance.AudioManager.PlaySound("Spin", false);
                    playButton.interactable = false;
                    ToggleLockBars(false);
                    game.UpdateBalance();
                    game.StartGame(new StartGameTicketDTO
                    {
                        GameId = game.GameID,
                        BetValue = game.SelectedBidAmount,
                        AttemptsCount = game.SelectedAttemptsCount,
                    });
                });
            }
            else if (game.IsGameStarted)
            {
                playText.SetLocalizedString(new LocalizeStringEntry("Take", localizeTable));
                playButton.onClick.RemoveAllListeners();
                playButton.onClick.AddListener(() =>
                {
                    App.Instance.AudioManager.PlaySound("Spin", false);
                    playButton.interactable = false;
                    ToggleCells(false);
                    game.EndGame();
                });
            }
        }

        private protected override void GameStartedHandle(IDataResponse dto)
        {
            game.UpdateBalance();
            SwitchPlayButtonState();
            ToggleCells(true);
        }

        private protected override void GetStepCellHandle(IDataResponse dto)
        {
            currentProfitInfo.gameObject.SetLocalizedString(new LocalizeStringEntry("Current_win", localizeTable));
            var stepResult = (GetStepResultDTOResponse)dto;
            var parentObject = cells[stepResult.Row, stepResult.Col];
            var text = parentObject.GetComponentInChildren<TextMeshProUGUI>();
            currentProfit.text = (ConvertCurrency(game.CurrentBid) * game.TotalMultiplier).ToString("F2");
            attemptsCount.text = (game.SelectedAttemptsCount - game.CurrentGameStep - 1).ToString();
            if (stepResult.Type == TicketCellState.Multiplier)
            {
                text.enabled = true;
                var value = (float)Math.Round(stepResult.Value, 2);
                text.text = value.ToString("F", CultureInfo.InvariantCulture);
                text.color = value > 1 ? Color.red : Color.black;
            }

            game.CurrentGameStep++;

            if (game.IsGameResolved)
            {
                isResetAvailable = true;
                ToggleCells(false);
                SwitchPlayButtonState();
                game.EndGame();
            }
        }

        private protected override void EndGameHandle(IDataResponse dto)
        {
            if (!isResetAvailable)
            {
                PrepareGameToStart();
            }
        }

        private void ToggleCells(bool enable)
        {
            for (int i = 0; i < game.FieldSizeX; i++)
            {
                for (int j = 0; j < game.FieldSizeY; j++)
                {
                    if (game[i, j].CellState == TicketCellState.Unknown)
                    {
                        cells[i, j].GetComponentInChildren<Button>().interactable = enable;
                    }
                }
            }
        }

        private void ToggleLockBars(bool enable)
        {
            attemptsMinus.gameObject.SetActive(enable);
            attemptsPlus.gameObject.SetActive(enable);
            bidMinus.gameObject.SetActive(enable);
            bidPlus.gameObject.SetActive(enable);
        }

        private void SetupFieldContent()
        {
            for (int i = 0; i < game.FieldSizeX; i++)
            {
                for (int j = 0; j < game.FieldSizeY; j++)
                {
                    if (cells[i, j] != null)
                    {
                        Destroy(cells[i, j].gameObject);
                    }

                    var row = i;
                    var col = j;
                    cells[i, j] = Instantiate(unknownCellPrefab, fieldContent);
                    var button = cells[i, j].GetComponentInChildren<Button>();
                    button.onClick.AddListener(() =>
                    {
                        App.Instance.AudioManager.PlaySound("MouseClickBoop", false);
                        game.GetStepResult(new GetStepResultTicketDTO
                        {
                            GameId = game.GameID,
                            Row = row,
                            Col = col,
                            UnixTimestamp = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds(),
                        });
                        StartErase(cells[row, col]);
                        button.onClick.RemoveAllListeners();
                    });
                }
            }
        }

        private void InitCellBackgrounds()
        {
            foreach (var cellBackgroundPrefab in cellBackgroundPrefabs)
            {
                cellBackgrounds.Add(cellBackgroundPrefab);
            }
        }

        private IEnumerator ToDestroyByTime(GameObject buttonObject, float time)
        {
            yield return new WaitForSeconds(time);

            Destroy(buttonObject);
        }

        private void StartErase(GameObject parentObject)
        {
            if (clickedCells >= game.SelectedAttemptsCount)
            {
                return;
            }

            clickedCells++;
            var button = parentObject.GetComponentInChildren<Button>();
            var cellPrefab = cellBackgrounds[UnityEngine.Random.Range(0, cellBackgrounds.Count - 1)];
            var cell = Instantiate(cellPrefab, parentObject.transform);
            cell.transform.SetAsFirstSibling();
            button.GetComponent<Animator>().SetTrigger("Erase");
            button.GetComponent<Animator>().SetBool("Erased", true);
            button.transform.SetParent(fieldContent.parent);
            var animationClips = button.GetComponent<Animator>().runtimeAnimatorController.animationClips;
            var animationTime = animationClips.Where(clip => clip.name == "Erase").First().length;
            button.StartCoroutine(ToDestroyByTime(button.gameObject, animationTime));
        }
    }
}
