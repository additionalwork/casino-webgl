mergeInto(LibraryManager.library, {

  GetGameSettingsWebGL: function (data, callback) {
    var stringData = UTF8ToString(data);

    // insert js method call:
    var serverResponse = "" // GetGameSettings(data);

    {{{ makeDynCall('vi', 'callback') }}}(serverResponse);
  },

  GetBalanceWebGL: function (callback) {

    // insert js method call:
    var serverResponse = "" // GetBalance();

    {{{ makeDynCall('vi', 'callback') }}}(serverResponse);
  },

  StartGameWebGL: function (data, callback) {
    var stringData = UTF8ToString(data);

    // insert js method call:
    var serverResponse = "" // StartGame(data);

    {{{ makeDynCall('vi', 'callback') }}}(serverResponse);
  },

  GetStepResultWebGL: function (data, callback) {
    var stringData = UTF8ToString(data);

    // insert js method call:
    var serverResponse = "" // GetClickResult(data);

    {{{ makeDynCall('vi', 'callback') }}}(serverResponse);
  },

  EndGameWebGL: function (data, callback) {
    var stringData = UTF8ToString(data);

    // insert js method call:
    var serverResponse = "" // EndGame(data);

    {{{ makeDynCall('vi', 'callback') }}}(serverResponse);
  },

});
