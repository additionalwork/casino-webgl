# Связь с приложением и библиотекой генерации последовательностей для игр

## Подключение библиотеки и пример использования

Для связи с библиотекой необходимо установить пакет pythonnet.

```bash
pip install pythonnet
```

Также для работы библиотеки необходима установленная платформа Mono 6.12.0.

Установка:

```bash
sudo apt install dirmngr gnupg apt-transport-https ca-certificates software-properties-common
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
sudo apt-add-repository 'deb https://download.mono-project.com/repo/ubuntu stable-focal main'

sudo apt install mono-complete
```

Подключение dll и вызов методов:

```python
import clr
import os
import json

pathDLL = os.getcwd() + "/CasinoGenerators/CasinoGenerators.dll"

clr.AddReference(pathDLL)

from CasinoGeneratorsLib import Generator

# received in request
gameid = "Sapper"
generator = Generator(gameid)

config_sapper = { "Coefficients": [ 0.1, 0.25, 0.5, 0.75, 1, 2, 2.5, 3, 5, 7.5], "CellsCount": 25, "EmptyProbability": 0.3 }
print(generator.SetConfig(json.dumps(config_sapper)))

received_start_game_DTO = { "MinesCount": 12, "BetValue": 100 }
winrate = 1
maxMultiplier = 100
sequence_response = generator.GenerateSequence(json.dumps(received_start_game_DTO), winrate, maxMultiplier)

print(sequence_response)
print(generator.GetNextStepResult())
print(generator.GetNextStepResult())
print(generator.GetNextStepResult())

print(generator.GetBalanceResponse(1200))
```

## Общие концепты

Каждая игра на WebGL связывается с фронтом и вызывает необходимые js методы (из созданной библиотеки .jslib). В параметрах игра передает соответсвующий вызванному методу и конкретной игре DTO в виде JSON и ожидает DTO в ответ.

Предполагается, что фронт отправляет полученные данные на определенные эндпоинты на бэк.

Для рассчета результатов игры (или последовательности результатов) и генерации ответных DTO вызывается соответсвующий метод из созданной библиотеки-генератора (предполагается использование библиотеки прямо на сервере, развернутой в виде dll, например).

При любом запросе, кроме "GetBalance" бэкенд получает id игры, а так же при запросе "StartGame" получает величину ставки. Остальные полученные данные бэкенд передает в генератор, в соотвествующий метод.

Хранение и использование конкретных DTO каждой игры на сервере не обязательно - это зона ответственности генераторов.

## Конструктор

*public SapperGenerator(double[] coefs, int cellsCount, double emptyProbability)*

- *coefficients* - массив доступных коэффициентов.
- *cellsCount* - общее количество клеток сапера.
- *emptyProbability* - шанс получения пустой клетки.

Предусмотрен коструктор без параметров, по умолчанию выставлены значения:
- coefficients = [ 0.1, 0.25, 0.5, 0.75, 1, 2, 2.5, 3, 5, 7.5 ];
- cellsCount = 25;
- emptyProbability = 0.3;

## Запрос "GetGameSettings"

При получении запроса бэкенд инстанцирует экземпляр библиотеки.

Для получения строки ответа, вызывается метод:

*string GetGameSettingsResponse(long minBetValue, long betStep, int minBombs, int maxBombs)*

В параметрах необходимо передать настройки для конкретной игры. В частности:

- *minBetValue* - минимальная ставка пользователя.
- *betStep* - шаг увеличения ставки.
- *minBombs* - минимальное значение бомб, которое может выбрать игрок.
- *maxBombs* - максимальное значение бомб, которое может выбрать игрок.

В текущей реализации генератора при винрейте 1 игрок в среднем выходит в минус начиная с 11 бомб. При 21 бомбе и больше, учитывая фиксированные коэфициенты, выиграть практически невозможно. В соотвествии с этим, предлагаем установить:

- *minBombs* = 11.
- *maxBombs* = 20.

Возвращенную из метода строку отправить в ответ.

## Запрос "StartGame"

Для получения строки ответа, вызывается метод:

*void GenerateSequence(string data, float winRate = 1, float maxMultiplier = -1)*

- *WinRate* - параметр отражающий коэффициент ожидаемой прибыли игрока (прим. при WinRate = 1 - игрок в среднем остается с таким же банком, какой и был; WinRate = 0.7 - на большой дистанции игрок теряет 30% от ставки). N.B. В текущей реализации winrate кореллирует с частотой и величиной выигрышей игрока, но из-за неопределенности некоторых факторов (отсутсвия гибкости коэффициентов, различное количество бомб, различные тактики игрока - риск, консерватизм или случайность) он не отражает точного числового значения выигрыша. 
- *MaxMultiplier* - минимальный полученный пользователем коэффициент, при котором он гарантировано получит мину (прим. при MaxMultiplier = 1.5 - в ход, при котором игрок должен был достигнуть коэффициента 1.5 или больше, игрок получает мину).
- *Data* - строка json (StartGameSapperDTO) полученного при запросе "StartGame".

Список возможных коэффициентов вписан напрямую в генератор и при получении параметра WinRate и в соответствии с ним для каждого коэффициента (пустой ячейки и бомбы включительно) выставляется соответствующий шанс выпадения пользователю.

## Запрос "GetNextStepResult"

*string GetNextStepResult()*

Метод вызывается при очередном выборе ячейки игрового поля.

## Дополнительные запросы

Для отвязывания логики бэкенда от функционирования приложения, предлагаем методы конструирующие строку ответа для остальных запросов, не связанных с генерацией ходов игры:

- *string GetBalanceResponse(long balance)* - запрос получения баланса.
- *string GetEndGameResponse(bool isSuccessful)* - запрос завершения игры.
