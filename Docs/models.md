# Общие модели

## Получение настроек

В общией модели полей не предусмотрено.

## Получение баланса игрока

Ответ:

```c#
class GetBalanceDTOResponse
{
    long Balance;
}
```

## Старт игры

В общей модели данных в запросе не предусмотрено.

Ответ:

```c#
class StartGameDTOResponse
{
    bool IsSuccessful;
    string ErrorMessage;
}
```

## Запрос результатов попытки

```c#
class GetStepResultDTO
{
    long UnixTimestamp;
}
```

Ответ:

```c#
class GetStepResultDTOResponse
{
    float Value;
}
```

## Сохранение результатов

Ответ:

```c#
class EndGameDTOResponse
{
    bool IsSuccessful;
}
```

# Модели игры "Сапер"

Примеры отправляемых и ожидаемых в ответ DTO в виде JSON для игры "Сапер" приведены ниже.

## Получение настроек

Ответ:

```c#
class GetSettingsSapperDTOResponse
{
    bool IsActiveSession;
    GetClickResultDTOResponse[] StepsHistory;
}
```

## Получение баланса игрока

Ответ:

```c#
class GetBalanceDTOResponse
{
    long Balance;
}
```

## Старт игры

```c#
class StartGameSapperDTO
{
    int MinesCount;
}
```

Ответ:

```c#
class StartGameDTOResponse
{
    bool IsSuccessful;
    string ErrorMessage;
}
```

## Запрос результатов попытки

```c#
class GetStepResultSapperDTO
{
    int Row;
    int Column;
    long UnixTimestamp;
}
```

Ответ:

```c#
class GetStepResultSapperDTOResponse
{
    SapperCellType Type;
    float Value;
}

enum SapperCellType
{
    Bomb = 0,
    Empty = 1,
    Multiplier = 2,
}
```

## Сохранение результатов

Ответ:

```c#
class EndGameDTOResponse
{
    bool IsSuccessful;
}
```